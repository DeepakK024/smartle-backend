const pool = require('../dbconnection/dbcon.js');


const query = (queryString, params) =>{
    return pool.promise().query(queryString,params);
    
}

module.exports =query;