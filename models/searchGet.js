const query = require('../utility/query');
var { ExecutionM } = require('./masterCommunication');
var { Execution } = require('./getUserCommunication');
var { isSessionActive } = require('./updateVRLogs');
var moment = require('moment');
const pool = require('../dbconnection/dbcon.js');

module.exports = {
    createSessionComm: async function (sessionId, userId, isDefault) {
        try {
            let nowTimeStamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
            const connection = await pool.promise().getConnection();
            var data
            var objSession = {
                "sessionId": sessionId
            }
            if (isDefault == 1) {
                //get master comm
                data = await ExecutionM();
            }
            else {
                //get user comm
                data = await Execution(objSession);
            }
            var sessionActive = await isSessionActive(sessionId);
            var result;
            if (sessionActive) {

                await connection.query('START TRANSACTION');
                var queryIs = `Insert into session_communication (response, masterCommunicationId, userId, sessionId, createdBy, createdAt, updatedBy, updatedAt, deletedAt) values(`;
                for (var i = 0; i < data.length; i++) {
                    if (i + 1 !== data.length) {
                        queryIs = queryIs + `'${data[i].response}',"${data[i].masterCommunicationId}", "${userId}", "${sessionId}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}", "${nowTimeStamp}"),(`
                    } else {
                        queryIs = queryIs + `'${data[i].response}',"${data[i].masterCommunicationId}", "${userId}", "${sessionId}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}", "${nowTimeStamp}");`
                    }
                }
                if (data.length) {
                    result = await connection.execute(queryIs);
                    connection.commit();
                    connection.release();
                    return result;
                }
                else {
                    console.log("no communication data recieved");
                    connection.rollback();
                    connection.release();
                }

            }
            else {
                console.log("session is inactive");
            }
        } catch (e) {
            console.log("error for the query ", e);
            return e;
        }

    }
}