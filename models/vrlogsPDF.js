// 'use strict';

const fs = require('fs');
const query = require("../utility/query.js");
var PdfTable = require('voilab-pdf-table')

async function getNurse(sessionId){
    const [result] = await query(`select nurseId from session where id = ?`, sessionId);
    return result;
}
async function getVRLogs(sessionId){
    const [result] = await query(`select statement,dateTime from vr_logs where sessionId = ?`, sessionId);
    return result;
}
//voi

// in some service
var PdfDocument = require('pdfkit');
module.exports = {
    create: async function (sessionId) {
        //nurse of the session
        const nurse = await getNurse(sessionId);
        //vr logs 
        const vrlogs = await getVRLogs(sessionId);
        var filename = "VR_Logs_";
        filename+=sessionId;
        filename+="_";
        filename+=nurse[0].nurseId
        filename+=".pdf";
        // create a PDF from PDFKit, and a table from PDFTable
        var pdf = new PdfDocument ({
            size: 'LEGAL', 
            info: {
              Title: 'VR Logs for the session',
              Author: 'Deepak',
            },
            autoFirstPage: false
            })
        pdf.pipe(fs.createWriteStream(filename));
        var table = new PdfTable(pdf, {
                bottomMargin: 30
            });
        table
            // add some plugins (here, a 'fit-to-width' for a column)
            .addPlugin(new (require('voilab-pdf-table/plugins/fitcolumn'))({
                column: 'VRLogs'
            }))
            // set defaults to your columns
            .setColumnsDefaults({
                headerBorder: 'B',
                align: 'right'
            })
            // add table columns
            .addColumns([
                {
                    id: 'VRLogs',
                    header: 'VR Logs',
                    align: 'left'
                },
                {
                    id: 'Time',
                    header: 'Time',
                    align: 'right',
                    width: 70
                }
            ])
            // add events (here, we draw headers on each new page)
            .onPageAdded(function (tb) {
                tb.addHeader();
            });

        var allVRLogs =[]
        if(vrlogs.length>=1){
            for(let i=0;i<vrlogs.length;i++){
                VRLOG={
                    VRLogs:vrlogs[i].statement,
                    Time:vrlogs[i].dateTime
                }
                allVRLogs.push(VRLOG);
            }
        }
        // if no page already exists in your PDF, do not forget to add one
        pdf.addPage();
        // draw content, by passing data to the addBody method
        table.addBody(allVRLogs);
        pdf.end();
        return pdf;
    }
};
