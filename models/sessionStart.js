const query = require('../utility/query');
var {createSessionComm} = require('./searchGet');
var {Execution} = require('./sessionEnd');

module.exports = {
    Execution: async function (data) {
        try {
            var scenario_control_entry;
            var scenario_control_data_id;
            var data;
            var [checker] = await query(`
            select id
            from scenerio_control_data
            where scenarioId = ${data.scenarioId}`);

            if (checker.length === 0) {  //If scenario control data is not present
                scenario_control_entry = await query(`    
                 insert into scenerio_control_data(trainerId,scenarioId,airways,exposure,deficit,vitalSigns,miscellaneous,breathing,circulation,genitourinary,gastrointestinal,composition)
                 values(${data.trainerId},${data.scenarioId},"","","","","","","","","","");`);
                const [getSCId] = await query(`
                    select id
                    from scenerio_control_data
                    where scenarioId = ${data.scenarioId}`);

                scenario_control_data_id = getSCId;
            }
            else {
                scenario_control_data_id = checker; //If scenario control data is already present
            }
            var result_obj;
            if (data.vr === 0) {
                await module.exports.endIfSession(data.trainerId);
                result_obj = await module.exports.forFront(data, scenario_control_data_id);
            }
            else {
                result_obj = await module.exports.forVR(data, checker);
            }
            return result_obj;
        }
        catch (error) {
            console.log("error ", error);
            return error;
        }
    },
    forFront: async function (data, scenario_control_data_id) {
        const session_insertion = await module.exports.getSessionInsert(data, scenario_control_data_id);
        const insertSessionComm = await createSessionComm(session_insertion.insertId, data.trainerId, data.isDefault);
        const results = await module.exports.SessionValues(data.isDefault, data.scenarioId);
        const session_info = await module.exports.getSessionInfo(data, scenario_control_data_id);
        const master_manikin_id = await module.exports.getMasterManikinId(data);
        const manikin_data = await module.exports.getManikin(master_manikin_id);
        const nurse_info = await module.exports.getNurseInfo(data);
        const scenario_detail = await module.exports.getScenarioDetail(data);
        const result_obj = await module.exports.getResultObj(nurse_info, manikin_data, results, scenario_detail, session_info, data);
        return result_obj;
    },
    getSessionInsert: async function (data, scenario_control_data_id) {
        const [session_insertion] = await query(`
        insert into session (trainerId,nurseId,scenerioControlDataId,isActive,isDefault)
        values(${data.trainerId},${data.nurseId},${scenario_control_data_id[0].id},1,${data.isDefault})`);
        return session_insertion;
    },
    getSessionInfo: async function (data, scenario_control_data_id) {
        const [session_info] = await query(`select id as sessionId
        from session
        where scenerioControlDataId = ${scenario_control_data_id[0].id} and trainerId = ${data.trainerId} and nurseId = ${data.nurseId} and isActive = 1`);
        return session_info;
    },
    getMasterManikinId: async function (data) {
        const [master_manikin_id] = await query(`select masterManikinId 
        from scenario
        where id = ${data.scenarioId}`);
        return master_manikin_id;
    },
    getManikin: async function (master_manikin_id) {
        const [manikin_data] = await query(`select name as manikinName, gender, age, ethnicity,id
        from master_manikin
        where id = ${master_manikin_id[0].masterManikinId}`);
        return manikin_data;
    },
    getNurseInfo: async function (data) {
        const [nurse_info] = await query(`select concat(firstName,lastName) as nurseName, id as nurseId
        from user 
        where id =${data.nurseId}`);
        return nurse_info;
    },
    getScenarioDetail: async function (data) {
        const [scenario_detail] = await query(`select * 
        from scenario 
        where id  = ${data.scenarioId}`);
        return scenario_detail;
    },
    forVR: async function (data, checker) {
        var id = checker[0]['id'];
        const [isDefault] = await query(`select isDefault, id as sessionId
        from session 
        where scenerioControlDataId = ${id} and isActive = 1 and nurseId = ${data.nurseId}`);
        
        if(!isDefault){
            return;
        }
        const is_default = isDefault[0]['isDefault'];

        var results = await module.exports.SessionValues(is_default, data.scenarioId);
        var result_obj = await module.exports.getResultVR(results, data.scenarioId);
        return result_obj;
    },
    SessionValues: async function (isDefault, scenarioId) {
        var results;
        try {

            if (isDefault == 1) {
                [results] = await query(`
                select *
                from  master_dashboard 
                where id = 1`);
            }
            if (isDefault == 0) {
                [results] = await query(`
                select * 
                from scenerio_control_data
                where scenarioId = ${scenarioId}`);
            }
            return results;
        }
        catch (err) {
            console.log("error in session values", err);
        }
    },
    getResultObj: async function (nurse_info, manikin_data, results, scenario_detail, session_info, data) {
        try {

            let result_obj = {
                nurseInfo:
                {
                    nurseName: nurse_info[0]['nurseName'],
                    nurseId: nurse_info[0]['nurseId']
                },
                manikinInfo:
                {
                    manikinName: manikin_data[0]['manikinName'],
                    gender: manikin_data[0]['gender'],
                    age: manikin_data[0]['age'],
                    ethnicity: manikin_data[0]['ethnicity'],
                    manikinId: manikin_data[0]['id'],
                },
                sessionInfo:
                    await module.exports.getParsedSessionInfo(results, data.scenarioId),
                scenarioDetail: {
                    scenarioName: scenario_detail[0]['scenarioName'],
                    patientName: scenario_detail[0]['patientName'],
                    dob: scenario_detail[0]['dob'],
                    mrn: scenario_detail[0]['mrn'],
                    height : scenario_detail[0]['height'],
                    weight : scenario_detail[0]['weight'],
                    hospitalName: scenario_detail[0]['hospitalName'],
                    reasonForAdmission: scenario_detail[0]['reasonForAdmission'],
                    currentStatus: scenario_detail[0]['currentStatus'],
                    recommendations: scenario_detail[0]['recommendations'],
                    otherAllergies: scenario_detail[0]['otherAllergies'],
                    masterManikinId: scenario_detail[0]['masterManikinId']
                },
                sessionId: session_info[0]['sessionId'],
                isActive: 1,
                trainerId: data.trainerId,
                nurseId: data.nurseId,
                isDefault: data.isDefault,
                scenarioId: data.scenarioId,
            }
            return result_obj;
        } catch (e) {
            console.log("error ", e);
        }
    },
    getResultVR: async function (results, scId) {
        var result_obj = await module.exports.getParsedSessionInfo(results, scId);
        return result_obj;
    },


    getParsedSessionInfo: async function (results, scId) {
        try {
            let sessionInfo = { name: "i am deafult info" };
            sessionInfo = Object.assign({}, {
                airways: module.exports.getJsonParsed(results[0], 'airways'),
                exposure: module.exports.getJsonParsed(results[0], 'exposure'),
                deficit: module.exports.getJsonParsed(results[0], 'deficit'),
                vitalSigns: module.exports.getJsonParsed(results[0], 'vitalSigns'),
                miscellaneous: module.exports.getJsonParsed(results[0], 'miscellaneous'),
                breathing: module.exports.getJsonParsed(results[0], 'breathing'),
                circulation: module.exports.getJsonParsed(results[0], 'circulation'),
                genitourinary: module.exports.getJsonParsed(results[0], 'genitourinary'),
                gastrointestinal: module.exports.getJsonParsed(results[0], 'gastrointestinal'),
                composition: module.exports.getJsonParsed(results[0], 'composition'),
                vrLogs: [],
            })
            var getIvSites = await module.exports.theIVSites(scId);
            var getWoundsDressing = await module.exports.theWoundsDressing(scId);
            sessionInfo.exposure.ivSites=getIvSites[0];
            sessionInfo.exposure.woundsDressing=getWoundsDressing[0];
            return sessionInfo;
        } catch (e) {
            console.log("yahan ====>", e);
        }
    },
    theIVSites:async function(scId){
        const result = query(`select iv.id, iv.location, iv.appearance from iv_sites as iv where scenarioId=?`,scId);
        return result;
    },
    theWoundsDressing: async function (scId){
        const result = query(`select wd.id, wd.type, wd.location, wd.appearance
        from wounds_dressing as wd where scenarioId=?`,scId);
        return result;
    },
    getJsonParsed: function (result, name) {
        try {
            console.log("result for json ",result[name]);
            let tempObj = result[name] ? JSON.parse(result[name]) : null;
            return tempObj ? tempObj : { "message": "data is incorrect" };
        }
        catch (e) {
            return { "message": "data is error" }
        }
    },
    isNurseActive: async function(id){
        const [result]  = await query(`select * from session where isActive=1 and nurseId=${id}`);
        return result;
    },
    endIfSession: async function(userId){
        const result = module.exports.sessionActiveOfUser(userId);
        if(result){
            const sessionClosed = await Execution(result[0]);
            await module.exports.killAllSessions(userId);
        }
    },
    sessionActiveOfUser: async function(userId){
        const [result] = await query(`select id from session where isActive=1 and (trainerId = ${userId} or nurseId=${userId})`);
        return result;
    },
    killAllSessions: async function(trainer){
        const [result] = await query(`update session 
            set isActive  = 0
            where trainerId = ${trainer}`);
            return result;
    }



}
