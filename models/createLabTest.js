var query = require("../utility/query.js");
const pool = require('../dbconnection/dbcon.js');
var moment = require ('moment');

module.exports = {

    getData: async function (data, userId) {
            const connection = await pool.promise().getConnection();
            try {
                await connection.query('START TRANSACTION');
                let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
                var dateTime = new Date(data['description']['dob']);
                var date_of_brith = dateTime.toISOString().slice(0, 19).replace('T', ' ');
                var updated = await connection.query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",patientName="${data['description']['patientName']}",masterManikinId="${data['description']['masterManikinId']}",dob="${date_of_brith}",mrn="${data['description']['mrn']}",height="${data['description']['height']}",weight="${data['description']['weight']}",hospitalName = "${data['description']['hospitalName']}", isPublic=${data['description']['isPublic']}, updatedBy="${userId}", updatedAt="${nowTimeStamp}" WHERE id = "${data['scenarioId']}" `);
                if (updated) {
                   await connection.query(`delete from lab_tests where scenarioId=${data['scenarioId']} and type = ${data.type}`);
                    if(!data.labTest.length){
                        connection.commit();
                        connection.release();
                        return true
                    }
                    var temp = `INSERT INTO lab_tests (scenarioId,type,dateTime,value,labTestName,criticalValue,createdAt,updatedAt,createdBy,updatedBy) VALUES (`;
                    for (var i = 0; i < data.labTest.length; i++) {
                        var labDateTime = new Date(data.labTest[i].date);
                        var labTestDate = labDateTime.toISOString().slice(0, 19).replace('T', ' ');

                        if (i + 1 !== data.labTest.length) {
                            temp = temp + `"${data['scenarioId']}","${data.type}","${labTestDate}","${data.labTest[i].value}","${data.labTest[i].labTestName}","${data.labTest[i].criticalValue}","${nowTimeStamp}","${nowTimeStamp}","${userId}","${userId}"),(`
                        } else {
                            temp = temp + `"${data['scenarioId']}","${data.type}","${labTestDate}","${data.labTest[i].value}","${data.labTest[i].labTestName}","${data.labTest[i].criticalValue}","${nowTimeStamp}","${nowTimeStamp}","${userId}","${userId}");`
                        }
                    }
                    if (data.labTest.length) {
                        var created;
                        created = await connection.query(temp);
                        if (created) {
                            connection.commit();
                            connection.release();
                            return true;

                        } else {
                            throw new Error('Error accur while creating lab results');
                        }
                    } else {
                        connection.commit();
                        connection.release();
                        console.log("commiting===1");
                        return true
                    }
                } else {
                    throw new Error('Error accur while updating scenrio');
                }
            } catch (error) {
                console.log("error in inner catch = ", error.stack);
                connection.rollback();
                connection.release();
                return error.message;
            }
    },
    deleteAlreadyData: async function(scId, type){
        try{
            const [result] = await query(`delete from lab_tests where scenarioId=${scId} and type = ${type}`);
            return result;
        }catch(e){
            console.log("error ", e.message);
        }
    }
};
