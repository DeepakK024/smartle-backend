//This file would contain the queries that would be used to get the scenario list back using the given length and offset.
var query = require("../utility/query");

var dataWithCount;
var count = 0;
async function scenarioListQuery(data, userId) {
    try {
        var date_of_birth = ''
        if(data.filter.dob!=''){
            var dateTime = new Date(data.filter.dob);
            date_of_birth = dateTime.toISOString().slice(0, 19).replace('T', ' ');
        }
        
        const results = await getScenarioList(data, date_of_birth, userId);
        return results;
            

        
    }
    catch (error) {
        console.log("error in func ", error);
        return error;
    }
}
async function getScenarioList(data, dob, userId){
    try{
        const [result] = await query(`SELECT
        S.id as id,S.scenarioName, S.patientName, S.dob, S.mrn, S.isPublic, S.createdBy as userId, M.name as manikinName, M.ethnicity, count(*) OVER () as TotalCount
        FROM scenario as S
        inner Join master_manikin as M on S.masterManikinId = M.id 
        where
        ${data.filter ? ` ${data.filter.scenarioName ? 'S.scenarioName like "%' + data.filter.scenarioName + '%" and ' : ''}
        ${data.filter.patientName ? 'S.patientName like "%' + data.filter.patientName + '%" and ' : ''} 
        ${dob ? 'S.dob like "%' + dob + '%" and ' : ''}
        ${data.filter.mrn ? 'S.mrn like "%'+ data.filter.mrn+'%" and ' : ''}
        ${data.filter.manikinName ? 'M.name like "%'+data.filter.manikinName+'%" and ' : ''}
        ${data.filter.ethnicity ? 'M.ethnicity like "%'+data.filter.ethnicity+'%" and ' : ''} ` : ''}
        S.deletedAt  is null and 
        (S.isPublic=1 or S.createdBy=${userId})
        order by S.createdAt desc   
        LIMIT ${data.pagination.limit} OFFSET ${data.pagination.offset};
        `);
            var dataWithCount;
            if(result.length){
                dataWithCount = {
                    scenarioList: result,
                    totalCount: result[0]['TotalCount']
                }
            }
            else {
                dataWithCount = {
                    scenarioList: result,
                    totalCount: 0
                }
            }
            return dataWithCount;
    }catch(e){
        console.log("error for list ", e);
    }
    
}

module.exports = { scenarioListQuery };

