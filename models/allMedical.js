const query =  require('../utility/query');

async function Execution(data) {
    try{
        var results;
        if(data.search === "")
        {
            [results] = await query(`select id, description from master_medical_history order by description limit 5`);
        }
        else{
        var arr = await data.search.split(" ");
        [results] = await query(`select id, description from master_medical_history where
            ((description REGEXP "${arr[0]}" ))`);

        }
        return results;
    }
    catch(err){
        console.log("error in query ", err);
        return err;
    }
}
module.exports = {Execution};