var query = require("../utility/query.js");

module.exports = {
    getData: async function (data, res) {
        try {
            var rows = await query(`select id, date, description, scenarioId from surgical_history as u
             where u.scenarioId=${data['scenarioId']}`);
            return rows;
        } catch (e) {
            console.log("error");
            console.log(e);
        }
    },

};