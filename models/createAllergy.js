const pool = require('../dbconnection/dbcon.js');
var moment = require ('moment');

module.exports = {

    getData: async function (data, userId) {
            const connection = await pool.promise().getConnection();
            try {
                let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
                await connection.query('START TRANSACTION');
                var dateTime = new Date(data['description']['dob']);
                var date_of_birth = dateTime.toISOString().slice(0, 19).replace('T', ' ');
                var updated = await connection.query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",patientName="${data['description']['patientName']}",dob="${date_of_birth}",mrn="${data['description']['mrn']}",height="${data['description']['height']}",weight="${data['description']['weight']}", hospitalName = "${data['description']['hospitalName']}",otherAllergies="${data['otherAllergy']}", isPublic=${data['description']['isPublic']}, updatedBy= "${userId}", updatedAt="${nowTimeStamp}" WHERE id = "${data['scenarioId']}" `);
                if (updated) {
                    var deletedAllergies = [];
                    var newAddedAllergies = [];
                    for (var i = 0; i < data.allergies.length; i++) {
                        if (data.allergies[i].type == 'add') {
                            newAddedAllergies.push(data.allergies[i]);
                        }
                        else if (data.allergies[i].type == 'deleted') {
                            deletedAllergies.push(data.allergies[i]);
                        }
                    }
                    var temp = `INSERT INTO allergy (scenarioId,masterAllergyId,reaction, createdBy, createdAt, updatedBy, updatedAt, deletedAt) VALUES (`;
                    for (var i = 0; i < newAddedAllergies.length; i++) {
                        if (i + 1 !== newAddedAllergies.length) {
                            temp = temp + `"${data['scenarioId']}","${newAddedAllergies[i].allergyId}","${newAddedAllergies[i].reaction}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}", null),(`
                        } else {
                            temp = temp + `"${data['scenarioId']}","${newAddedAllergies[i].allergyId}","${newAddedAllergies[i].reaction}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}", null);`
                        }
                    }
                    if (data.allergies.length) {
                        var created;
                        var deletedArray;
                        if (newAddedAllergies.length != 0) {
                            created = await connection.query(temp);
                        } else {
                            created = true;
                        }

                        if (created) {
                            var temp2 = `DELETE FROM allergy WHERE id IN (`
                            for (var i = 0; i < deletedAllergies.length; i++) {
                                if (i + 1 !== deletedAllergies.length) {
                                    temp2 = temp2 + `"${deletedAllergies[i].id}",`
                                } else {
                                    temp2 = temp2 + `"${deletedAllergies[i].id}");`
                                }
                            }
                            if (deletedAllergies.length != 0) {
                                deletedArray = await connection.query(temp2);
                            }
                            else {
                                deletedArray = true;
                            }

                            if (deletedArray) {
                                connection.commit();
                                connection.release();
                                return true;

                            } else {
                                throw new Error('Error accur while deleting allergy');
                            }


                        } else {
                            throw new Error('Error accur while creating allergy');
                        }
                    } else {
                        connection.commit();
                        connection.release();
                        return true;
                    }
                } else {
                    throw new Error('Error accur while updating scenrio');
                }
            } catch (error) {
                console.log("error in inner catch = ", error.stack);
                connection.rollback();
                connection.release();
                return error.message;
            }
    },

};
