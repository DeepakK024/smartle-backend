const query = require(`../utility/query`);

module.exports = {
    Execution : async function (data)
    {
        try{
            const [check] = await query(`select scenerioControlDataId
                                 from session 
                                 where id = ${data.sessionId}`);
            const id = check[0].scenerioControlDataId;
            const [result] = await query(`update scenerio_control_data
            set airways = '${JSON.stringify(data.airways)}', exposure = '${JSON.stringify(data.exposure)}', deficit = '${JSON.stringify(data.deficit)}', vitalSigns = '${JSON.stringify(data.vitalSigns)}', miscellaneous = '${JSON.stringify(data.miscellaneous)}', breathing = '${JSON.stringify(data.breathing)}', circulation = '${JSON.stringify(data.circulation)}', genitourinary = '${JSON.stringify(data.genitourinary)}', gastrointestinal = '${JSON.stringify(data.gastrointestinal)}', composition = '${JSON.stringify(data.composition)}'
            where id = ${id}`);
            return result;
        }
        catch(err){
            return err;
        }
    }
}