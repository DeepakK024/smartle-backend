const query = require("../utility/query.js");

module.exports = {
    getDevicesList: async function (data) {
        try {
            const [rows] = await query(`select deviceName, info from devices where scenarioId=?`, data);
            return rows;
        } catch (e) {
            console.log("error");
            console.log(e);
        }
    },
    parseInfo:async function(data){
        for (let i=0;i<data.length;i++){
            data[i].info= JSON.parse(data[i].info);
        }
        return data;
    }
};