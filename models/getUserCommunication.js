const query =  require('../utility/query');

module.exports = {
    Execution : async function (data){
        try{
            const [id] = await query(`select scenerioControlDataId as id
            from session
            where id = ${data.sessionId}`);
            const scenario_control_id = id[0].id;
            const [results] = await query(`select sc.id, sc.response, m.question,m.type, sc.masterCommunicationId, m.category, sc.userId, sc.scenerioControlDataId
            from scenerio_communication sc join master_communication as m on sc.masterCommunicationId=m.id
            where scenerioControlDataId = ${scenario_control_id}`);
            return results;
        }
        catch(err){
            return err;
        }
    }
}