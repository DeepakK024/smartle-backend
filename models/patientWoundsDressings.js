const query = require("../utility/query.js");
const pool = require('../dbconnection/dbcon.js');
var moment = require ('moment');

module.exports = {
    getWoundsDressings:async function (data){
        try{
            const [rows] = await query(`select wd.id, wd.type, wd.location, wd.appearance
             from wounds_dressing as wd where scenarioId=?`,data);
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    },
    insertWoundsDressing : async function (data, userId){
        const connection = await pool.promise().getConnection();
        try{
            let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
            await connection.query('START TRANSACTION');
            var dateTime = new Date(data['description']['dob']);
            var date_of_brith = dateTime.toISOString().slice(0, 19).replace('T', ' ');
            var updated = await connection.query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",patientName="${data['description']['patientName']}",masterManikinId="${data['description']['masterManikinId']}",dob="${date_of_brith}",mrn="${data['description']['mrn']}",height="${data['description']['height']}",weight="${data['description']['weight']}", hospitalName = "${data['description']['hospitalName']}", isPublic=${data['description']['isPublic']}, updatedBy="${userId}", updatedAt="${nowTimeStamp}" WHERE id = "${data['scenarioId']}" `);
            if (updated) {
                await connection.query(`delete from wounds_dressing where scenarioId=${data['scenarioId']}`);
                if(!data['woundsDressing'].length){
                    connection.commit();
                    connection.release();
                    return true
                }
                else{
                    var temp= `insert into wounds_dressing ( type, location, appearance, scenarioId, createdBy,createdAt,updatedBy,updatedAt,deletedAt) values(`;
                    for (var i = 0; i < data['woundsDressing'].length; i++) {
                        var tempDate= new Date();
                        var dateTime=tempDate.toISOString().slice(0, 19).replace('T', ' ');
                        if (i + 1 !== data['woundsDressing'].length) {
                            temp = temp + `"${data['woundsDressing'][i].type}","${data['woundsDressing'][i].location}","${data['woundsDressing'][i].appearance}", "${data.scenarioId}", "${userId}", "${nowTimeStamp}","${userId}", "${nowTimeStamp}", null),(`
                        } else {
                            temp = temp + `"${data['woundsDressing'][i].type}","${data['woundsDressing'][i].location}","${data['woundsDressing'][i].appearance}", "${data.scenarioId}", "${userId}", "${nowTimeStamp}","${userId}", "${nowTimeStamp}", null);`
                        }
                    }
                    if( data['woundsDressing'].length!=0)
                    {
                        response=await connection.execute(temp);
                    }
                    if(response[0].insertId){
                        connection.commit();
                        connection.release();
                        return  true;
                    }
                    else {
                        throw new Error('Error occur while creating wounds and dressing');
                    }

                }
            }else {
                throw new Error('Error accur while updating scenrio');
            }
        }
        catch (error) {
            console.log("error in inner catch = ", error.stack);
            connection.rollback();
            connection.release();
            return error.message;
        }
    },
    deleteAlreadyRecords: async function(scId){
        var isDeleted = await query(`delete from wounds_dressing where scenarioId=${scId}`);
        return isDeleted;
    }
};
