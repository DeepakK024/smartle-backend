const query =  require('../utility/query');

async function Execution(data) {
    try{
        var results;
        if(data.search === ""){
            [results] = await query(`select distinct u.id as nurseId, CONCAT(u.firstName ," ", u.lastName) as nurseName, u.email as email , u.isActive as active 
            from user as u join role as r on u.id=r.userId
            where u.id not in (select distinct nurseId from session where isActive=1) and u.isActive=1 and r.masterRolesId=3 limit 5`)
        }
        else{
        var arr = await data.search.split(" ");
        if (arr[1] !== undefined){
            [results] = await query(`select distinct u.id as nurseId, CONCAT(u.firstName ," ", u.lastName) as nurseName, u.email as email , u.isActive as active 
            from user as u join role as r on u.id=r.userId
            where u.id not in (select distinct nurseId from session where isActive=1) and u.isActive=1 and r.masterRolesId=3 and
             ((u.firstName REGEXP "${arr[0]}" ) OR (u.lastName REGEXP "${arr[1]}"))`);
        }else{
            [results] = await query(`select distinct u.id as nurseId, CONCAT(u.firstName ," ", u.lastName) as nurseName, u.email as email , u.isActive as active 
            from user as u join role as r on u.id=r.userId
            where u.id not in (select distinct nurseId from session where isActive=1) and u.isActive=1 and r.masterRolesId=3 and
             ((u.firstName REGEXP "${arr[0]}" ))`);
        }
    }
    return results;
    }
    catch(err){
        return err;
    }
}
module.exports = {Execution};