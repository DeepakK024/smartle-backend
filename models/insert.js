const query = require("../utility/query.js");
const moment = require ('moment');

module.exports = {
    insertAuth : async function (ID,token, nowTimeStamp,delAt=null){
        try {
            let dateExpire=moment(new Date(Date.now()+ (24*60*60*1000))).format('YYYY-MM-DD HH:mm:ss');
            var response=await query(`insert into user_auth (id, token, timeToLive, userId, createdBy,createdAt,updatedBy,updatedAt,deletedAt)
            values(null, "${token}", "${dateExpire}", ${ID},${ID}, "${nowTimeStamp}", ${ID}, "${nowTimeStamp}", null)`);
            ID = response[0].insertId;
            return ID;
        }catch(e){
            console.log(e);
        }
        
    },
    updateActive : async function (id){
        try{
            await query(`UPDATE user set isActive=1 where id=?`,id);     
        }catch(e){
            console.log("error");
            console.log(e);
        }
    },
    getAuthToken: async function (id){
        const [rows] = await query(`select token, timeToLive from user_auth where userId=${id}`);
        return rows;
    }
}