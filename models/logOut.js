var query = require("../utility/query.js");
var {Execution} = require('./sessionEnd');
module.exports = {
    getData: async function (data, res) {
        try {
            var sessionId = await module.exports.isSessionExisting(data.userid);
            if(sessionId.length){
                var deleteData = await Execution(sessionId[0].id);
            }
            var rows = await query(`DELETE FROM user_auth WHERE token ="${data.authorization}"`);
            if (rows[0].affectedRows!=0) {
                var updated = await query(`Update user Set isActive="${0}" WHERE id = "${data.userid}"`);
                if(updated){
                    return true;
                }  
            }else{
                return false;
            }
        } catch (e) {
            console.log("error");
            console.log(e);
        }
    },
    isSessionExisting: async function(userId){
        const [results] = await query(`select id as sessionId from session where (trainerId = ${userId} or nurseId=${userId}) and isActive = 1`);
        return results;
    }

};