const query = require("../utility/query.js");

module.exports = {
    getManikin:async function (){
        try{
            const [rows] = await query(`select m.id, m.name as manikinName, m.age, m.ethnicity, m.gender from master_manikin as m`);
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    }
};
