// const query = require('../utility/query');
// var moment = require ('moment');
 
// async function Copy(data, userId) {
//     try{
//         let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
//         const result = await getNewScId(data, userId, nowTimeStamp);
//         try{
//             const [results] = await query(`
//             insert into iv_sites (location, appearance, scenarioId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select location, appearance, ${result.insertId} ,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from iv_sites
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in ivSites ", e.message);
//         }
        
        
//         try{
//             const [allergy] = await query(`
//             insert into allergy (masterAllergyId, scenarioId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select masterAllergyId, ${result.insertId},${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from allergy
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in allergy ", e.message);
//         }
        

//         try{
//             const [surgical] = await query(`
//             insert into surgical_history (date, description, scenarioId, createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select date, description, ${result.insertId},${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from surgical_history
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in surgical history ", e.message);
//         }
        

//         try{
//             const [medical] = await query(`
//             insert into medical_history (date, scenarioId,masterMedicalHistoryId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select date, ${result.insertId},masterMedicalHistoryId,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from  medical_history
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in medical history ", e.message);
//         }
        

//         try{
//             const [labTests] = await query(`
//             insert into lab_tests (labTestName, criticalValue,value,dateTime,scenarioId,type,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select labTestName, criticalValue, value,dateTime,${result.insertId},type,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from  lab_tests
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in lab tests ", e.message);
//         }
        

//         try{
//             const [wounds] = await query(`
//             insert into wounds_dressing (location, type,scenarioId,appearance,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select location, type,${result.insertId},appearance,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from  wounds_dressing
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in wounds ", e.message);
//         }
        

//         try{
//             const [bands] = await query(`
//             insert into wrist_bands (hand, type,scenarioId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
//             select hand, type,${result.insertId},${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
//             from  wrist_bands
//             where scenarioId = ${data.scenarioId};`);
//         }catch(e){
//             console.log("error in bands ", e.message);
//         }
    
//     return result;
//     }
//     catch(err)
//     {
//         return err;
//     }  
// }
// async function getNewScId(data, userId, nowTimeStamp){
//     try{
//         const [result] = await query(`insert into scenario (scenarioName, patientName, dob, mrn, hospitalName, isPublic, reasonForAdmission, currentStatus,recommendations,otherAllergies,createdBy, createdAt, updatedBy,updatedAt,deletedAt, masterManikinId)
//         select "${data.scenarioName}", patientName, dob, mrn, hospitalName, isPublic, reasonForAdmission, currentStatus,recommendations,otherAllergies,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null, masterManikinId
//          from scenario
//          where id = ${data.scenarioId};`);
//         return result;
//     }catch(e){
//         console.log("error in getting new sc id ", e.message);
//     }
// }

// module.exports = {Copy};



var moment = require ('moment');
const pool = require('../dbconnection/dbcon.js');

//Doing with transaction
async function Copy(data, userId) {
    const connection = await pool.promise().getConnection();
    try{
        await connection.query('START TRANSACTION');
        let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        const [result] = await connection.query(`insert into scenario (scenarioName, patientName, dob, mrn, hospitalName, isPublic, reasonForAdmission, currentStatus,recommendations,otherAllergies, height, weight,createdBy, createdAt, updatedBy,updatedAt,deletedAt, masterManikinId)
        select "${data.scenarioName}", patientName, dob, mrn, hospitalName, isPublic, reasonForAdmission, currentStatus,recommendations,otherAllergies,height,weight,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null, masterManikinId
         from scenario
         where id = ${data.scenarioId};`);
        
         const [results] = await connection.query(`
         insert into iv_sites (location, appearance, scenarioId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select location, appearance, ${result.insertId} ,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from iv_sites
         where scenarioId = ${data.scenarioId};`);

         const [allergy] = await connection.query(`
         insert into allergy (masterAllergyId, scenarioId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select masterAllergyId, ${result.insertId},${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from allergy
         where scenarioId = ${data.scenarioId};`);

         const [surgical] = await connection.query(`
         insert into surgical_history (date, description, scenarioId, createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select date, description, ${result.insertId},${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from surgical_history
         where scenarioId = ${data.scenarioId};`);

         const [medical] = await connection.query(`
         insert into medical_history (date, scenarioId,masterMedicalHistoryId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select date, ${result.insertId},masterMedicalHistoryId,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from  medical_history
         where scenarioId = ${data.scenarioId};`);

         const [labTests] = await connection.query(`
         insert into lab_tests (labTestName, criticalValue,value,dateTime,scenarioId,type,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select labTestName, criticalValue, value,dateTime,${result.insertId},type,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from  lab_tests
         where scenarioId = ${data.scenarioId};`);

         const [wounds] = await connection.query(`
         insert into wounds_dressing (location, type,scenarioId,appearance,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select location, type,${result.insertId},appearance,${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from  wounds_dressing
         where scenarioId = ${data.scenarioId};`);
    
         const [bands] = await connection.query(`
         insert into wrist_bands (hand, type,scenarioId,createdBy, createdAt, updatedBy,updatedAt,deletedAt)
         select hand, type,${result.insertId},${userId}, "${nowTimeStamp}", ${userId}, "${nowTimeStamp}", null
         from  wrist_bands
         where scenarioId = ${data.scenarioId};`);


        connection.commit();
        connection.release();

        return bands;
    }catch(e){
        console.log("error in transaction");
        connection.rollback();
        connection.release();
        return e;
    }




}

module.exports = {Copy};
