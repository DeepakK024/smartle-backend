const query = require('../utility/query');
var data_strc;

async function medicalList(){
    try{
        const [results] = await query(`select * from master_medical_history order by description`);
        var count = results.length;
        data_strc = {
            data : results,
            Total : count
        }
        return data_strc
    }catch(err){
        return err
    }
}

module.exports = {medicalList}