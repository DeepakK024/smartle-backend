const query = require('../utility/query');
var moment = require('moment');
var { isSessionActive } = require('./updateVRLogs');
const pool = require('../dbconnection/dbcon.js');

module.exports = {
    Execution: async function (data, userId) {
        try {

            let nowTimeStamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
            const connection = await pool.promise().getConnection();
            if (!data.communicationData.length) {
                console.log("no data recieved, sending back");
                return;
            }
            else {
                var sessionActive = await isSessionActive(data.sessionId);
                if (sessionActive[0].isActive == 1) {
                    const id = await module.exports.getScControl(data);
                    const scenario_control_data_id = id[0].scId;
                    var deleted = module.exports.deleteScenarioCommunication(scenario_control_data_id);
                    if (deleted) {
                        await connection.query('START TRANSACTION');
                        var queryIs = `Insert into scenerio_communication (response, masterCommunicationId, userId, scenerioControlDataId, createdBy, createdAt, updatedBy, updatedAt, deletedAt) values(`;
                        for (var i = 0; i < data.communicationData.length; i++) {
                            if (i + 1 !== data.communicationData.length) {
                                queryIs = queryIs + `'${JSON.stringify(data.communicationData[i].response)}',"${data.communicationData[i].masterCommunicationId}", "${userId}", "${scenario_control_data_id}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}","${nowTimeStamp}"),(`
                            } else {
                                queryIs = queryIs + `'${JSON.stringify(data.communicationData[i].response)}',"${data.communicationData[i].masterCommunicationId}", "${userId}", "${scenario_control_data_id}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}","${nowTimeStamp}");`
                            }
                        }
                        if (data) {
                            var result = await connection.execute(queryIs);
                            //result = await query(queryIs);
                            console.log("result ", result)
                            connection.commit();
                            connection.release();
                            return result;
                        }
                        else {
                            console.log("query failed ");
                            connection.rollback();
                            connection.release();
                        }
                    }
                    else {
                        console.log("could not delete the scenario communication");
                    }
                }
                else {
                    console.log("session is inActive");
                }
            }
        }
        catch (err) {
            return err;
        }
    },
    deleteScenarioCommunication: async function (id) {
        const [result] = await query(`delete from scenerio_communication where scenerioControlDataId = ${id}`);
        return result
    },
    getScControl: async function (data) {
        const [id] = await query(`select scenerioControlDataId as scId
            from session
            where id = ${data.sessionId}`);
        return id;
    }
}