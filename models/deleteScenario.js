var query = require("../utility/query");

async function deleteScenario(data)
{
    try{
    var [result] = await query(`update scenario
    set deletedAt = current_time()
    where id = ${data.scenarioId}`);
    }catch(err){
        return err;
    }
}

module.exports = {deleteScenario};

