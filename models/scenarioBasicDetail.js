const query = require('../utility/query');

async function basicDetailsQuery(data) {
    try{
    const [results] = await query(`select s.id,s.scenarioName, s.patientName,m.name, s.dob, s.mrn
    , s.height, s.weight, s.hospitalName, s.reasonForAdmission,s.currentStatus,m.ethnicity,m.gender,
    s.otherAllergies,s.recommendations, m.id as manikinId, m.age as manikinAge, s.isPublic, s.createdBy as userId
    from scenario as s
    inner join master_manikin as m on s.masterManikinId = m.id
    where s.id = ${data.scenarioId}`);
    return results;
    }
    catch(error)
    {
        return error;
    }
}
module.exports = basicDetailsQuery
