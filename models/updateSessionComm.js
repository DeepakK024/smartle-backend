const query = require('../utility/query');
var moment = require ('moment');
var {isSessionActive} = require('./updateVRLogs');

module.exports = {
    insertComm : async function (data, userId){
        try{
            let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
            if(!data.communicationData.length){
                console.log("no data recieved, sending back");
                return;
            }
            else {
                var sessionActive = await isSessionActive(data.sessionId);
                if(sessionActive){
                    var deleted = module.exports.deleteSessionCommunication(data.sessionId);
                    if(deleted){
                        var queryIs = `Insert into session_communication (response, masterCommunicationId, userId, sessionId, createdBy, createdAt, updatedBy, updatedAt, deletedAt) values(`;
                        for (var i = 0; i < data.communicationData.length; i++) {
                            if (i + 1 !== data.communicationData.length) {
                                queryIs = queryIs + `'${JSON.stringify(data.communicationData[i].response)}',"${data.communicationData[i].masterCommunicationId}", "${userId}", "${data.sessionId}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}", "${nowTimeStamp}"),(`
                            } else {
                                queryIs = queryIs + `'${JSON.stringify(data.communicationData[i].response)}',"${data.communicationData[i].masterCommunicationId}", "${userId}", "${data.sessionId}", "${userId}", "${nowTimeStamp}", "${userId}", "${nowTimeStamp}", "${nowTimeStamp}");`
                            }
                        }
                        if(data){
                            result = await query(queryIs);
                        }
                        else {
                            console.log("query failed ");
                        }
                    }
                    else {
                        console.log("could not delete the session communication");
                    }
                }
                else {
                    console.log("session is inActive");
                }
            }
        }
        catch(err){
            return err;
        }
    },
    deleteSessionCommunication: async function(id){
        const [results] = await query(`delete from session_communication where sessionId = ${id}`);
        return results ;
    }
}