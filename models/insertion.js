var query = require("../utility/query.js");

module.exports = {
    updateScenario: async function(data, date_of_brith){
        const [result] = await query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",patientName="${data['description']['patientName']}",masterManikinId="${data['description']['masterManikinId']}",dob="${date_of_brith}",mrn="${data['description']['mrn']}",hospitalName = "${data['description']['hospitalName']}" WHERE id = "${data['scenarioId']}" `);
        return result;
    },
    updateScenarioMedicalNotes: async function(data, date_of_birth){
        const [result] =  await query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",masterManikinId="${data['description']['masterManikinId']}",patientName="${data['description']['patientName']}",dob="${date_of_birth}",mrn="${data['description']['mrn']}",hospitalName = "${data['description']['hospitalName']}",reasonForAdmission="${data['reasonForAdmission']}",currentStatus="${data['currentStatus']}",recommendations="${data['recommendations']}" WHERE id = "${data['scenarioId']}" `);
        return result;
    },
    updateScenarioAllergy: async function(data, date_of_birth){
        const [result] = await query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",patientName="${data['description']['patientName']}",dob="${date_of_birth}",mrn="${data['description']['mrn']}",hospitalName = "${data['description']['hospitalName']}",otherAllergies="${data['otherAllergy']}" WHERE id = "${data['scenarioId']}" `);
        return result;
    }
}