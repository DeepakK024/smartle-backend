var query = require("../utility/query.js");
const pool = require('../dbconnection/dbcon.js');
var moment = require ('moment');

module.exports = {

    getData: async function (data, userId) {
            const connection = await pool.promise().getConnection();
            try {
                await connection.query('START TRANSACTION');
                let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
                var dateTime = new Date(data['description']['dob']);
                var date_of_brith = dateTime.toISOString().slice(0, 19).replace('T', ' ');
                var updated = await connection.query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",patientName="${data['description']['patientName']}",masterManikinId="${data['description']['masterManikinId']}",dob="${date_of_brith}",mrn="${data['description']['mrn']}",height="${data['description']['height']}",weight="${data['description']['weight']}",hospitalName = "${data['description']['hospitalName']}", isPublic=${data['description']['isPublic']}, updatedBy="${userId}", updatedAt="${nowTimeStamp}" WHERE id = "${data['scenarioId']}" `);
                if (updated) {
                   await connection.query(`delete from devices where scenarioId=${data['scenarioId']}`);
                    if(!data.device.length){
                        connection.commit();
                        connection.release();
                        return true
                    }
                    var temp = `INSERT INTO devices (scenarioId,deviceName, info, createdAt,updatedAt,createdBy,updatedBy, deletedAt) VALUES (`;
                    for (var i = 0; i < data.device.length; i++) {
                        if (i + 1 !== data.device.length) {
                            temp = temp + `"${data['scenarioId']}","${data.device[i].deviceName}",'${JSON.stringify(data.device[i].info)}',"${nowTimeStamp}","${nowTimeStamp}","${userId}","${userId}", null),(`
                        } else {
                            temp = temp + `"${data['scenarioId']}","${data.device[i].deviceName}",'${JSON.stringify(data.device[i].info)}',"${nowTimeStamp}","${nowTimeStamp}","${userId}","${userId}", null);`
                        }
                    }
                    if (data.device.length) {
                        var created;
                        created = await connection.query(temp);
                        if (created) {
                            connection.commit();
                            connection.release();
                            return true;

                        } else {
                            throw new Error('Error occur while creating devices');
                        }
                    } else {
                        connection.commit();
                        connection.release();
                        console.log("commiting===1");
                        return true
                    }
                } else {
                    throw new Error('Error accur while updating scenrio');
                }
            } catch (error) {
                console.log("error in inner catch = ", error.stack);
                connection.rollback();
                connection.release();
                return error.message;
            }
    },
    deleteAlreadyData: async function(scId, type){
        try{
            const [result] = await query(`delete from lab_tests where scenarioId=${scId} and type = ${type}`);
            return result;
        }catch(e){
            console.log("error ", e.message);
        }
    }
};
