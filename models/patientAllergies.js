var query = require("../utility/query.js");

module.exports = {
    getAllergies: async function (data) {
        try {
            const [rows] = await query(`select a.id, ma.description, a.reaction, ma.type from allergy as a 
            join master_allergy as ma on ma.id=a.masterAllergyId where scenarioId=${data['scenarioId']} and ma.type=${data['type']}`);
            return rows;
        } catch (e) {
            console.log("error");
            console.log(e);
        }
    }
};
