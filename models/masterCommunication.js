const query = require(`../utility/query`);

module.exports = {
    ExecutionM : async function ()
    {
        try{
            const [results] = await query(`select id as masterCommunicationId, question, type, intentName, m.response, category from master_communication as m`)
            return results;
        }
        catch(err){
            return err;
        }
    }
}