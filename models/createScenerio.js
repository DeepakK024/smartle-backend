var query = require("../utility/query.js");
var moment = require ('moment');

module.exports = {
    getData:async function (data,user){
        try{
            let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
            var dateTime=new Date(data['dob']);
            var date=dateTime.toISOString().slice(0, 19).replace('T', ' ');
            var rows = await query(`INSERT INTO scenario (scenarioName,patientName,dob,mrn,height, weight, hospitalName, masterManikinId, isPublic, createdBy, createdAt, updatedBy, updatedAt, deletedAt) VALUES ("${data['scenarioName']}","${data['patientName']}","${date}","${data['mrn']}","${data['height']}","${data['weight']}","${data['hospitalName']}","${data['masterManikinId']}",${data['isPublic']}, ${user}, "${nowTimeStamp}", ${user}, "${nowTimeStamp}", null)`)
            return rows[0].insertId;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    },

};