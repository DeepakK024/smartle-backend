const query = require("../utility/query.js");
const md5 = require('md5');

module.exports = {
    getUser:async function (data){
        try{
            const userPasswordHash=md5(data['password']);
            const [rows] = await query(`select u.id as user_id, u.firstName, u.isActive,
            u.lastName, mr.id as role_id, mr.roleName, p.privileges from user as u
            join role as r on r.userId=u.id
            join master_roles as mr on r.masterRolesId=mr.id
            join master_privilege as p on mr.masterPrivilegeId=p.id 
            where u.email="${data['email']}" AND u.password = "${userPasswordHash}"`);
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    },
    getData:async function(result, token){
        let user = {
            "userId":result[0]['user_id'],
            "firstName":result[0]['firstName'],
            "lastName":result[0]['lastName'],
            "isActive":result[0]['isActive'],
        }
        let role = {
            "roleId":result[0]['role_id'],
            "roleName":result[0]['roleName']
        }
        let privileges = {
           "priviliges":result[0]['privileges']
        }
        let data = {
           "sessionToken":token,
           "user":user,
           "role":role,
           "priviliges":privileges
        }
        return data;
    },
    endAuth: async function(id){
        var [rows] = await query(`DELETE FROM user_auth WHERE userId = ${id}`)
        return rows
    }
};