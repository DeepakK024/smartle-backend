const query = require("../utility/query.js");

module.exports = {
    insertUpdateVR : async function (data,nowTimeStamp,userId){
        try {
            var response=await query(`insert into vr_logs (id, statement, dateTime, sessionId, createdBy,createdAt,updatedBy,updatedAt,deletedAt)
            values(null, "${data.statement}", "${data.time}", "${data.sessionId}", "${userId}", "${nowTimeStamp}","${userId}", "${nowTimeStamp}", "${nowTimeStamp}" )`);
        }catch(e){
            console.log(e);
        }
        let ivSite = response[0].insertId;
        return ivSite;
    },
    isSessionActive:async function (data){
        try{
            const [rows] = await query(`select isActive from session where id=?`,data);
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    }
};

