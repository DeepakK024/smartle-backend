const query = require("../utility/query.js");

module.exports = {
    getLabResults: async function (data) {
        try {
            const [rows] = await query(`select lr.id, lr.type, lr.labTestName,
            lr.criticalValue, lr.value, lr.dateTime as date from lab_tests as lr where scenarioId=?`, data);
            return rows;
        } catch (e) {
            console.log("error");
            console.log(e);
        }
    }
};
