const query = require('../utility/query');
var {deleteSessionCommunication} = require('./updateSessionComm');

module.exports = {
    Execution : async function(data)
    {
        try{
            var deleted = deleteSessionCommunication(data.sessionId);
            const [result] = await query(`update session 
            set isActive  = 0
            where id = ${data.sessionId}`); 
            return result;
        }
        catch(err){
            return err;
        }
    }
}