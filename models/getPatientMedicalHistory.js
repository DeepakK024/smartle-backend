var query = require("../utility/query.js");

module.exports = {
    getData: async function (data) {
        try {
            var rows = await query(`select u.id, u.date, u.masterMedicalHistoryId as medicalHistoryId, m.description from medical_history as u
             inner join master_medical_history as m on m.id=u.masterMedicalHistoryId
             where u.scenarioId="${data['scenarioId']}"`);
            return rows;
        } catch (e) {
            console.log("error");
            console.log(e);
        }
    },

};