var query = require("../utility/query.js");
const pool = require('../dbconnection/dbcon.js');
var moment = require ('moment');

module.exports = {

    getData: async function (data, userId) {
            const connection = await pool.promise().getConnection();
            try {
                let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
                await connection.query('START TRANSACTION');
                var dateTime = new Date(data['description']['dob']);
                var date_of_birth = dateTime.toISOString().slice(0, 19).replace('T', ' ');
                var updated = await connection.query(`Update scenario Set scenarioName="${data['description']['scenarioName']}",masterManikinId="${data['description']['masterManikinId']}",patientName="${data['description']['patientName']}",dob="${date_of_birth}",mrn="${data['description']['mrn']}",height="${data['description']['height']}",weight="${data['description']['weight']}",hospitalName = "${data['description']['hospitalName']}", reasonForAdmission="${data['reasonForAdmission']}",currentStatus="${data['currentStatus']}",recommendations="${data['recommendations']}", isPublic=${data['description']['isPublic']}, updatedBy="${userId}", updatedAt="${nowTimeStamp}" WHERE id = "${data['scenarioId']}" `);
                if (updated) {
                    var deletedMedicalHistory = [];
                    var newAddedMedicalHistory = [];
                    var deletedSurgicalHistory = [];
                    var newAddedSurgicalHistory = [];
                    if(!data.medicalHistory.length){
                        data.medicalHistory=[];
                    }
                    for (var i = 0; i < data.medicalHistory.length; i++) {
                        if (data.medicalHistory[i].type == 'add') {
                            newAddedMedicalHistory.push(data.medicalHistory[i]);
                        }
                        else if (data.medicalHistory[i].type == 'deleted') {
                            deletedMedicalHistory.push(data.medicalHistory[i]);
                        }
                    }

                    var temp = `INSERT INTO medical_history (scenarioId,masterMedicalHistoryId,date, createdBy, createdAt, updatedBy, updatedAt, deletedAt) VALUES (`;
                    for (var i = 0; i < newAddedMedicalHistory.length; i++) {
                        var dateTime = new Date(newAddedMedicalHistory[i].date);
                        var medicalDate = dateTime.toISOString().slice(0, 19).replace('T', ' ');
                        if(newAddedMedicalHistory[i].medicalHistoryId === null){
                            var newMasterKey;
                            var masterId;
                            newMasterKey = await module.exports.addedAlready(newAddedMedicalHistory[i].description);
                            if(!newMasterKey.length){
                                newMasterKey = await query(`insert into master_medical_history(description, createdBy, createdAt, updatedBy, updatedAt, deletedAt) values("${newAddedMedicalHistory[i].description}",${userId}, "${nowTimeStamp}",${userId},"${nowTimeStamp}", null )`)
                                masterId = newMasterKey[0].insertId;
                            }
                            else {
                                masterId = newMasterKey[0].id;
                            }
                            if (i + 1 !== newAddedMedicalHistory.length) {
                                temp = temp + `"${data['scenarioId']}","${masterId}","${medicalDate}", "${userId}","${nowTimeStamp}","${userId}","${nowTimeStamp}","${nowTimeStamp}"),(`
                            } else {
                                temp = temp + `"${data['scenarioId']}","${masterId}","${medicalDate}", "${userId}","${nowTimeStamp}","${userId}","${nowTimeStamp}","${nowTimeStamp}");`
                            }
                        }
                        else {
                            if (i + 1 !== newAddedMedicalHistory.length) {
                                temp = temp + `"${data['scenarioId']}","${newAddedMedicalHistory[i].medicalHistoryId}","${medicalDate}", "${userId}","${nowTimeStamp}","${userId}","${nowTimeStamp}","${nowTimeStamp}"),(`
                            } else {
                                temp = temp + `"${data['scenarioId']}","${newAddedMedicalHistory[i].medicalHistoryId}","${medicalDate}", "${userId}","${nowTimeStamp}","${userId}","${nowTimeStamp}","${nowTimeStamp}");`
                            }
                        }
                        
                    }
                    if (data.medicalHistory.length) {
                        var created;
                        var deletedArray;
                        if (newAddedMedicalHistory.length != 0) {
                            created = await connection.query(temp);
                        } else {
                            created = true;
                        }

                        if (created) {
                            var temp2 = `DELETE FROM medical_history WHERE id IN (`
                            for (var i = 0; i < deletedMedicalHistory.length; i++) {
                                if (i + 1 !== deletedMedicalHistory.length) {
                                    temp2 = temp2 + `"${deletedMedicalHistory[i].id}",`
                                } else {
                                    temp2 = temp2 + `"${deletedMedicalHistory[i].id}");`
                                }
                            }
                            if (deletedMedicalHistory.length != 0) {
                                deletedArray = await connection.query(temp2);
                            }
                            else {
                                deletedArray = true;
                            }

                            if (deletedArray) {
                                if(!data.surgicalHistory.length){
                                    data.surgicalHistory=[];
                                }
                                for (var j = 0; j < data.surgicalHistory.length; j++) {
                                    if (data.surgicalHistory[j].type == 'add') {
                                        newAddedSurgicalHistory.push(data.surgicalHistory[j]);
                                    }
                                    else if (data.surgicalHistory[j].type == 'deleted') {
                                        deletedSurgicalHistory.push(data.surgicalHistory[j]);
                                    }
                                }
                                var tempSurgicalAdd = `INSERT INTO surgical_history (scenarioId,description,date, createdBy, createdAt, updatedBy, updatedAt, deletedAt) VALUES (`;
                                for (var x = 0; x < newAddedSurgicalHistory.length; x++) {
                                    var dateTime = new Date(newAddedSurgicalHistory[x].date);
                                    var surgicalDate = dateTime.toISOString().slice(0, 19).replace('T', ' ');
                                    if (x + 1 !== newAddedSurgicalHistory.length) {
                                        tempSurgicalAdd = tempSurgicalAdd + `"${data['scenarioId']}","${newAddedSurgicalHistory[x].description}","${surgicalDate}", "${userId}","${nowTimeStamp}","${userId}","${nowTimeStamp}","${nowTimeStamp}"),(`
                                    } else {
                                        tempSurgicalAdd = tempSurgicalAdd + `"${data['scenarioId']}","${newAddedSurgicalHistory[x].description}","${surgicalDate}", "${userId}","${nowTimeStamp}","${userId}","${nowTimeStamp}","${nowTimeStamp}");`
                                    }
                                }
                                if (data.surgicalHistory.length) {
                                    var createdSurgical;
                                    var deletedArraySurgical;
                                    if (newAddedSurgicalHistory.length != 0) {
                                        createdSurgical = await connection.query(tempSurgicalAdd);
                                    } else {
                                        createdSurgical = true;
                                    }

                                    if (createdSurgical) {
                                        var tempSurgicalDelete = `DELETE FROM surgical_history WHERE id IN (`
                                        for (var a = 0; a < deletedSurgicalHistory.length; a++) {
                                            if (a + 1 !== deletedSurgicalHistory.length) {
                                                tempSurgicalDelete = tempSurgicalDelete + `"${deletedSurgicalHistory[a].id}",`
                                            } else {
                                                tempSurgicalDelete = tempSurgicalDelete + `"${deletedSurgicalHistory[a].id}");`
                                            }
                                        }
                                        if (deletedSurgicalHistory.length != 0) {
                                            deletedArraySurgical = await connection.query(tempSurgicalDelete);
                                        }
                                        else {
                                            deletedArraySurgical = true;
                                        }

                                        if (deletedArraySurgical) {
                                            connection.commit();
                                            connection.release();
                                            return true;
                                        } else {
                                            throw new Error('Error accur while deleteing surgical history');
                                        }


                                    } else {
                                        throw new Error('Error accur while creating surgical history');
                                    }
                                } else {
                                    connection.commit();
                                    connection.release();
                                    return true;
                                }
                            } else {
                                throw new Error('Error accur while deleteing medical history');
                            }


                        } else {
                            throw new Error('Error accur while creating medical history');
                        }
                    } else {
                        connection.commit();
                        connection.release();
                        return true;
                    }
                } else {
                    throw new Error('Error accur while updating scenrio');
                }
            } catch (error) {
                console.log("error in inner catch = ", error.stack);
                connection.rollback();
                connection.release();
                return error.message;
            }
    },
    addedAlready: async function(desc){
        const [result] = await query(`select id from master_medical_history where description = "${desc}"`);
        return result;
    },
    userAuth: async function(token){
        const [result] = await query(`select userId from user_auth where token = ?`,token);
        return result;
    },
    isUserSame: async function(id){
        const [result] = await query(`select createdBy from scenario where id = ?`,id);
        return result
    }
};
