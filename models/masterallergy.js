var query = require("../utility/query.js");

module.exports = {
    getMasterAllergy:async function (data){
        try{
            const [rows] = await query(`select ma.id, ma.description, ma.type from master_allergy as ma where ma.type=${data} order by ma.description`, data)
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    }
};
