//This file would contain the queries that would be used to get the scenario list back using the given length and offset.
var query = require("../utility/query");

var dataWithCount;
var count = 0;
async function scenarioListQuery(data) {
    try{
    count = 0;
    var [result] = await query(`SELECT
     S.scenario_name , S.patient_name, S.dob, S.mrn, M.name
    FROM scenario as S
    inner Join master_manikin as M on S.master_manikin_id = M.id
    where
    S.scenario_name like "%${data.filter.scenarioName}%" and 
    S.patient_name like "%${data.filter.patientName}%" and 
    S.dob like "%${data.filter.dob}%" and 
    S.mrn like "%${data.filter.mrn}%" and
    M.name like "%${data.filter.manikinName}%" and 
    M.ethnicity like "%${data.filter.ethnicity}%"         
    LIMIT ${data.pagination.length} OFFSET ${data.pagination.offset}`);

    dataWithCount = {
        data : result,
        data_count : result.length
    }
    return dataWithCount;
    }
    catch(error)
    {
        return error;
    }
}

module.exports = {scenarioListQuery};

