var query = require("../utility/query.js");
const _ = require("lodash");
module.exports = {
    getData: async function (data, res) {
        try {
            try {
                var [checked] = await query(`select * from session  where nurseId="${data.userid}" AND isActive=1`);
                // console.log("checked ===== ",checked)
                if (checked.length) {
                    console.log("1->");
                    var [temp] = await query(`select * from scenerio_control_data  where id="${checked[0].scenerioControlDataId}"`);
                    if (temp.length) {
                        console.log("2->");
                        try{
                            var [scenerioInfo] = await query(`select s.id as scenarioId,s.scenarioName,s.patientName,s.masterManikinId,s.dob,
                            s.mrn,s.height, s.weight,s.hospitalName, s.reasonForAdmission,s.currentStatus,s.recommendations,s.otherAllergies, ma.id as manikinId, ma.gender, ma.ethnicity
                            from scenario as s 
                            left join master_manikin as ma on s.masterManikinId = ma.id
                            
                            where s.id="${temp[0].scenarioId}"
                            `);
                            if(scenerioInfo){
                                try{
                                    var [labtestsRawData] = await query(`select labt.id as labTestId,labt.type as typeL,labt.labTestName as name,labt.criticalValue,labt.value,
                                    labt.dateTime from lab_tests as labt 
                                    where labt.scenarioId ="${temp[0].scenarioId}"
                                    `)
                                    // console.log("lbtestsRawdata ",labtestsRawData)

                                    var [medicalHistoryRawData] = await query(`select m.id as medicalHistoryId,
                                    m.date as dateM,masterm.description as medicalDescription
                                    from medical_history as m
                                    left join master_medical_history as masterm on m.masterMedicalHistoryId=masterm.id
                                    where m.scenarioId = "${temp[0].scenarioId}"
                                    `)
                                    

                                    var [allergiesRawData] = await query (`select a.id as allergyId,mar.description as allergyDesc,mar.type as allergyType, a.reaction
                                    from allergy as a 
                                    left join master_allergy as mar on a.masterAllergyId = mar.id
                                    where a.scenarioId = "${temp[0].scenarioId}"
                                    `)
                                    // console.log("allergies ",allergiesRawData)

                                    var [surgicalHistoryRawData] = await query(`select su.id as surgicalId,su.date as dateS,su.description as surgicalDescription
                                    from surgical_history as su 
                                    where su.scenarioId = "${temp[0].scenarioId}"
                                    `)

                                    var [woundsDressingRawData] = await query(`select wd.id as woundsDressingId,wd.type as typeWD,wd.location as locationWD,wd.appearance as appearanceWD
                                    from wounds_dressing as wd
                                    where wd.scenarioId = "${temp[0].scenarioId}"
                                    `)

                                    var [devicesRawData] = await query(`select
                                    dvc.id as deviceId, dvc.deviceName, dvc.info
                                    from devices as dvc
                                    where dvc.scenarioId = "${temp[0].scenarioId}"
                                    `)

                                    var [ivSitesRawData] = await query(`select ivs.id as ivSiteId,
                                    ivs.location as ivLocation,ivs.appearance as appearanceIV
                                    from iv_sites as ivs 
                                    where ivs.scenarioId = "${temp[0].scenarioId}"
                                    `)

                                    var [wristBandsRawData] = await query(`select wb.id as wristBandId,wb.type as typeWB,wb.hand
                                    from wrist_bands as wb 
                                    where wb.scenarioId = "${temp[0].scenarioId}"
                                    `)
                                }
                                catch(e){
                                    console.log("error in query for other tables ",e);
                                }


                            }
                        }catch(e){
                            console.log("error in query ",e);
                        }
                        // var scenerioDescription = _.uniqBy(scenerioInfo, 'scenarioId');
                        // var medical_history = _.uniqBy(scenerioInfo, 'medicalHistoryId');
                        // var surgical_history = _.uniqBy(scenerioInfo, 'surgicalId');
                        // var lab_results = _.uniqBy(scenerioInfo, 'labTestId');
                        // var iv_sites = _.uniqBy(scenerioInfo, 'ivSiteId');
                        // var wounds_dressing = _.uniqBy(scenerioInfo, 'woundsDressingId');
                        // var wirst_bands = _.uniqBy(scenerioInfo, 'wristBandId');
                        // var allergy = _.uniqBy(scenerioInfo, 'allergyId');
                        // var manikin  = _.uniqBy(scenerioInfo, 'manikinId');
                        // var devicesList  = _.uniqBy(scenerioInfo, 'deviceId');


                        var scenerioDescription = scenerioInfo;
                        var medical_history = medicalHistoryRawData;
                        var surgical_history = surgicalHistoryRawData;
                        var lab_results = labtestsRawData;
                        var iv_sites = ivSitesRawData;
                        var wounds_dressing = woundsDressingRawData;
                        var wirst_bands = wristBandsRawData;
                        var allergy = allergiesRawData;
                        var devicesList  = devicesRawData;
                        let description = [];
                        let medicalHistory = [];
                        let surgicalHistory = [];
                        let labResult = [];
                        let ivSites = [];
                        let woundDressing = [];
                        let wristBands = [];
                        let devices = [];
                        let allergyInfo = [];
                        let data = {};
                        // console.log("obj ",scenerioDescription);
                        // console.log("length ",scenerioDescription.length);
                        if (scenerioDescription.length) {
                            console.log("3->");
                            for (var i = 0; i < scenerioDescription.length; i++) {
                                
                                var tempDes = {
                                    "scenarioId": scenerioDescription[i].scenarioId,
                                    "scenarioName": scenerioDescription[i].scenarioName,
                                    "patientName": scenerioDescription[i].patientName,
                                    "masterManikinId": scenerioDescription[i].masterManikinId,
                                    "dob": scenerioDescription[i].dob,
                                    "mrn": scenerioDescription[i].mrn,
                                    "height" : scenerioDescription[i].height,
                                    "weight" : scenerioDescription[i].weight,
                                    "hospitalName":scenerioDescription[i].hospitalName,
                                    "reasonForAdmission": scenerioDescription[i].reasonForAdmission,
                                    "currentStatus": scenerioDescription[i].currentStatus,
                                    "recommendations": scenerioDescription[i].recommendations,
                                    "otherAllergies": scenerioDescription[i].otherAllergies,
                                    "gender":scenerioDescription[i].gender,
                                    "ethnicity":scenerioDescription[i].ethnicity
                                }
                                description.push(tempDes);
                            }
                            data['description'] = description;
                        } else {
                            console.log("in else 7");
                        }
                        if (medical_history.length && medical_history[0].medicalHistoryId != null) {
                            console.log("4->");
                            for (var i = 0; i < medical_history.length; i++) {
                                var tempMedicalHistory = {
                                    "medicalHistoryId": medical_history[i].medicalHistoryId,
                                    "date": medical_history[i].dateM,
                                    "medicalDescription": medical_history[i].medicalDescription,
                                }
                                medicalHistory.push(tempMedicalHistory);
                            }
                            data['medicalHistory'] = medicalHistory;
                        } else {
                            console.log("in else 6");
                        }
                        if (surgical_history.length && surgical_history[0].surgicalId != null) {
                            console.log("5->");
                            for (var i = 0; i < surgical_history.length; i++) {
                                var tempSurgicalHistory = {
                                    "surgicalId": surgical_history[i].surgicalId,
                                    "date":surgical_history[i].dateS,
                                    "surgicalDescription": surgical_history[i].surgicalDescription,
                                }
                                surgicalHistory.push(tempSurgicalHistory);
                            }
                            data['surgicalHistory'] = surgicalHistory;
                        } else {
                            console.log("in else 5");
                        }
                        if (lab_results.length && lab_results[0].labTestId!=null) {
                            console.log("6->");
                            for (var i = 0; i < lab_results.length; i++) {
                                // console.log("type ",lab_results[i].typeL)
                                var tempLabResult = {
                                    "labTestId": lab_results[i].labTestId,
                                    "type": lab_results[i].typeL,
                                    "labTestName": lab_results[i].name,
                                    "criticalValue": lab_results[i].criticalValue,
                                    "value": lab_results[i].value,
                                    "dateTime": lab_results[i].dateTime,
                                }
                                labResult.push(tempLabResult);
                            }
                            data['labResults'] = labResult;
                        } else {
                            console.log("in else 4");
                        }
                        if (iv_sites.length && iv_sites[0].ivSiteId != null) {
                            console.log("7->");
                            for (var i = 0; i < iv_sites.length; i++) {
                                var tempIvSites = {
                                    "ivSiteId": iv_sites[i].ivSiteId,
                                    "location": iv_sites[i].ivLocation,
                                    "appearance": iv_sites[i].appearanceIV,
                                }
                                ivSites.push(tempIvSites);
                            }
                            data['ivSites'] = ivSites;
                        } else {
                            console.log("in else 3");
                        }
                        if (wounds_dressing.length && wounds_dressing[0].woundsDressingId != null) {
                            console.log("8->");
                            for (var i = 0; i < wounds_dressing.length; i++) {
                                var tempWoundsDressing = {
                                    "woundsDressingId": wounds_dressing[i].woundsDressingId,
                                    "type": wounds_dressing[i].typeWD,
                                    "location": wounds_dressing[i].locationWD,
                                    "appearance": wounds_dressing[i].appearanceWD,
                                }
                                woundDressing.push(tempWoundsDressing);
                            }
                            data['woundsDressing'] = woundDressing;
                        } else {
                            console.log("in else 2");
                        }
                        // console.log("wristBands ",wirst_bands)
                        if (wirst_bands.length && wirst_bands[0].wristBandId!=null) {
                            console.log("9->");
                            for (var i = 0; i < wirst_bands.length; i++) {
                                var tempWristBands = {
                                    "wristBandId": wirst_bands[i].wristBandId,
                                    "hand": wirst_bands[i].hand,
                                    "type":wirst_bands[i].typeWB
                                }
                                wristBands.push(tempWristBands);
                            }
                            data['wirstBands'] = wristBands;
                        } else {
                            console.log("in else 1");
                        }
                        if (devicesList.length && devicesList[0].deviceId != null) {
                            console.log("10->");
                            for (var i = 0; i < devicesList.length; i++) {
                                var tempDevice = {
                                    "deviceName": devicesList[i].deviceName,
                                    "info": JSON.parse(devicesList[i].info),
                                }
                                //console.log("info if valid ", tempDevice.info)
                                devices.push(tempDevice);
                            }
                            data['Devices'] = devices;
                        } else {
                            console.log("in else devices corrupted 0");
                        }

                        if (allergy.length) {
                            console.log("11->");
                            for (var i = 0; i < allergy.length; i++) {
                                var tempAllergy = {
                                    "allergyId": allergy[i].allergyId,
                                    "type" : allergy[i].allergyType,
                                    "description" : allergy[i].allergyDesc,
                                    "reaction": allergy[i].reaction,
                                }
                                if(allergy[i].allergyId !== null){
                                    allergyInfo.push(tempAllergy);
                                }
                            }
                            data['allergies'] = allergyInfo;
                        } else {
                            console.log("error here =============");
                        }
                        return data;
                    }
                    else {
                        console.log("error here ============= 0");
                        let data = {}
                        return data
                    }
                } else {
                    console.log("error here ============= 1");
                    let data = {}
                    return data
                }
            } catch (error) {
                console.log("error in inner catch = ", error.stack);
                return error.message;
            }
        } catch (e) {
            console.log("error in outer catch = ", e.stack);
            return e.message;
        }
    },
};
