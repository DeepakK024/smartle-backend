var query = require("../utility/query.js");

module.exports = {
    getToken:async function (data){
        try{
            const [rows] = await query(`select token from user_auth where userId = ?`, data);
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    },
    getSessionId: async function (data){
        try{
            const [rows] = await query(`select id from session where isActive=1 and trainerId = ?`, data);
            return rows;        
        }catch(e){
            console.log("error");
            console.log(e);
        } 
    },
    getScenarioId: async function(){
        try{
            const [rows] = await query(`select id from scenario where scenarioName="test101"`);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteMedicalData: async function(data){
        try{
            const [rows] = await query(`delete from medical_history where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteSurgicalData: async function(data){
        try{
            const [rows] = await query(`delete from surgical_history where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteAllergiesData: async function(data){
        try{
            const [rows] = await query(`delete from allergy where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteLabData: async function(data){
        try{
            const [rows] = await query(`delete from lab_tests where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteIVData: async function(data){
        try{
            const [rows] = await query(`delete from iv_sites where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteWoundsData: async function(data){
        try{
            const [rows] = await query(`delete from wounds_dressing where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteWristData: async function(data){
        try{
            const [rows] = await query(`delete from wrist_bands where scenarioId=?`,data);
            return rows; 
        }catch(e){
            console.log("error");
        }
    },
    deleteScenarioData: async function(data){
        try{
            const [rows] = await query(`delete from scenario where scenarioName="test102"`);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    deleteOtherData: async function(data){
        try{
            var scenarioCDId = await module.exports.getScenarioDC(data);
            var sessionIds = await module.exports.getSessionIds(scenarioCDId[0].id);
            await module.exports.deleteVRLogs(sessionIds[0].id);
            await module.exports.deleteSession(scenarioCDId[0].id);
            await module.exports.deleteScCom(scenarioCDId[0].id);
            await module.exports.deleteSC(data);
            await module.exports.deleteScenarioDataOther();

        }catch(e){

        }
    },
    getScenarioDC: async function(data){
        try{
            const [rows] = await query(`select id from scenerio_control_data where scenarioId=?`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    getSessionIds: async function(data){
        try{
            const [rows] = await query(`select id from session where scenerioControlDataId=?`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    deleteVRLogs: async function(data){
        try{
            const [rows] = await query(`delete from vr_logs where sessionId=?`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    deleteSession: async function(data){
        try{
            const [rows] = await query(`delete from session where scenerioControlDataId=?`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    deleteScCom: async function(data){
        try{
            const [rows] = await query(`delete from scenerio_communication where scenerioControlDataId=?`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    deleteSC: async function(data){
        try{
            const [rows] = await query(`delete from scenerio_control_data where scenarioId=?`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    },
    deleteScenarioDataOther: async function(data){
        try{
            const [rows] = await query(`delete from scenario where scenarioName="test101"`, data);
            return rows; 
        }catch(e){
            console.log("error",e);
        }
    }

};