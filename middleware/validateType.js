const Joi = require('joi')

module.exports ={
    validateType : async function(data){
        try{
            const checked = Joi.object({
                scenarioId:Joi.number().required(),
                 type: Joi.string().allow('',null).default("")
             })
             const result = checked.validateAsync(data);
             return result;
        }
        catch(e){
            console.log("error in valid ", e)
        }
       
    }

}