const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports={
    validateForgotPassword: function(postObj){
        const schema = Joi.object().keys({
           email:Joi.string().required()
        });
        const result = schema.validate(postObj);
        return result;
      }
}
