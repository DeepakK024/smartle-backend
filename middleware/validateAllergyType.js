const Joi = require('joi').extend(require('@hapi/joi-date'));
const validateAlType = Joi.object().keys({scenarioId:Joi.number().required(),
                                            type:Joi.number().required()})

module.exports = {validateAlType}