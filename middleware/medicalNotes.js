const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports = {
    validateMedicalNotes: function (postObj) {
        var schemaDesp = Joi.object().keys({
            scenarioName: Joi.string().required(),
            patientName: Joi.string().required(),
            masterManikinId: Joi.number().required(),
            dob: Joi.date().required(),
            mrn: Joi.string().required(),
            height:Joi.number().required(),
            weight: Joi.number().required(),
            hospitalName: Joi.string().required(),
            isPublic:Joi.number().required()

        });

        const schema = Joi.object().keys({
            scenarioId: Joi.number().required(),
            reasonForAdmission: Joi.string().allow('',null).default(""),
            currentStatus: Joi.string().allow('',null).default(""),
            recommendations: Joi.string().allow('',null).default(""),
            description: schemaDesp,
            medicalHistory: Joi.array(),
            surgicalHistory: Joi.array()
        });

        const result = schema.validate(postObj);
        return result;
    }
}