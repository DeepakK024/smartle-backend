const Joi  = require('joi');

module.exports = {
        validateCommunication : async function(data){
            const schema = Joi.object({
                sessionId : Joi.number().integer().required(),
                communicationData : Joi.array().required()
            });
            const result = schema.validateAsync(data);
            return result;
        }
}