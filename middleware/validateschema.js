const Joi = require('joi')//.extend(require('@hapi/joi-date'));

const scenarioBodySchema = Joi.object({         //This is the body of the Scenario list that must contain all of this

    
        limit : Joi.number()
                .integer()
                .min(0)
                .required(),
    offset :  Joi.number()
                 .min(0)
                 .required()
                
})

const masterMedHisSchema = Joi.object({
    limit : Joi.number()
    .integer()
    .min(0)
    .required(),
    offset :  Joi.number()
                  .min(0)
                  .required()
})

const scenarioDelSchema = Joi.object({
    scenarioId : Joi.number()
                    .integer()
                    .required()
})
const scenarioCopySchema = Joi.object({
    scenarioId : Joi.number()
                    .integer()
                    .required(),
    scenarioName : Joi.string().required()
})

const activeNursesIntellisence = Joi.object({
    search : Joi.required()
})

const sessionCheckData = Joi.object({
    userId : Joi.number()
                    .integer().required(),
                    
    trainerId : Joi.number() .integer().required(),

    vr : Joi.number()
                  .integer().required(),
    scenarioId : Joi.number()
                    .integer().required(),
    isDefault : Joi.number()
                    .integer().required()
})

const sessionEndCheck = Joi.object({
    sessionId : Joi.number().integer().required()
})
const sessionSaveCheck = Joi.object({
    sessionId : Joi.number().integer().required(),
    airways : Joi.required(),
    exposure : Joi.required(),
    deficit : Joi.required(),
    vitalSigns : Joi.required(),
    miscellaneous : Joi.required(),
    breathing :  Joi.required(),
    circulation : Joi.required(),
    genitourinary : Joi.required(),
    gastrointestinal : Joi.required(),
    composition : Joi.required()
})


module.exports= {sessionSaveCheck,sessionEndCheck,scenarioBodySchema, masterMedHisSchema, scenarioDelSchema ,scenarioCopySchema, activeNursesIntellisence, sessionCheckData };