const Joi = require('joi')

module.exports ={
    validateSessionValues : async function(data){
       const checked = Joi.object({
            scenarioId : Joi.number().integer().required(),
            userId : Joi.number().integer().required(),
            vr : Joi.number().integer().required()
        })
        const result = checked.validateAsync(data);
        return result;
    }

}