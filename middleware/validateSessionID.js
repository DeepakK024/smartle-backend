const Joi = require('joi')

module.exports = {
    validateSessionID : async function (data){
        const schema = Joi.object({
            sessionId : Joi.number().integer().default(null),
            isDefault : Joi.number().required()
        })
        const result =  schema.validateAsync(data);
        return result;
    }
}