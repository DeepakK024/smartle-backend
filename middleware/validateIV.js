const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports = {
        validateIVSite: function (postObj) {
        var schemaDesp = Joi.object().keys({
            scenarioName: Joi.string().required(),
            patientName: Joi.string().required(),
            masterManikinId: Joi.number().required(),
            dob: Joi.date().required(),
            mrn: Joi.string().required(),
            height:Joi.number().required(),
            weight: Joi.number().required(),
            hospitalName: Joi.string().required(),
            isPublic:Joi.number().required()

        });
        var ivSiteDesp = Joi.array().items({
                id:Joi.number().allow(null).default(null),
                location:Joi.string().default(null),
                appearance:Joi.string().default(null),

        });
        const schema = Joi.object().keys({
                ivSites:ivSiteDesp,
                description: schemaDesp,
                scenarioId:Joi.number().required(),
        });

        const result = schema.validate(postObj);
        return result;
    }
}