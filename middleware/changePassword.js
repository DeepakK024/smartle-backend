const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports={
    validateChangePassword: function(postObj){
        const schema = Joi.object().keys({
           oldPassword:Joi.required(),
           newPassword:Joi.string().required()
        });
        const result = schema.validate(postObj);
        return result;
      }
}
