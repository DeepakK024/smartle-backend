const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports={
    validateCreateScenerio: function(postObj){
        const schema = Joi.object().keys({
            scenarioName:Joi.string().required(),
            patientName:Joi.string().required(),
            masterManikinId:Joi.number().required(),
            dob:Joi.date().required(),
            mrn:Joi.string().required(),
            height:Joi.number().required(),
            weight: Joi.number().required(),
            hospitalName: Joi.string().required(),
            isPublic:Joi.number().required()
        });
        const result = schema.validate(postObj);
        return result;
      }
}