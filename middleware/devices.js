const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports = {
    validateDeviceInfo: function (postObj) {
        var schemaDesp = Joi.object().keys({
            scenarioName: Joi.string().required(),
            patientName: Joi.string().required(),
            masterManikinId: Joi.number().required(),
            dob: Joi.date().required(),
            mrn: Joi.string().required(),
            height: Joi.number().required(),
            weight: Joi.number().required(),
            hospitalName: Joi.string().required(),
            isPublic:Joi.number().required()

        });
        const detail = Joi.object().keys({
            deviceName: Joi.string().default(null),
            info: Joi.required(),
        });
        const schema = Joi.object().keys({
            scenarioId: Joi.number().required(),
            description: schemaDesp,
            device: Joi.array().items(detail.default(null).default(null)),
        });

        const result = schema.validate(postObj);
        return result;
    }
}