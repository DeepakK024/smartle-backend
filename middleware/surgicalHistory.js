const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports = {
    validateUserSurgicalHistory: function (postObj) {
        const schema = Joi.object().keys({
            scenarioId: Joi.number().required(),
        });
        const result = schema.validate(postObj);
        return result;
    }
}