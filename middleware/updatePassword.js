const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports={
    validateUpdatePassword: function(postObj){
        const schema = Joi.object().keys({
           oldPassword:Joi.string().required(),
           newPassword:Joi.string().required()
        });
        const result = schema.validate(postObj);
        return result;
      }
}
