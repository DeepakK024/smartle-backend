const Joi = require('joi')//.extend(require('@hapi/joi-date'));
const validateSc = Joi.object().keys({scenarioId:Joi.number().required()})
async function validateScenarioId(postObj){
    try{
        const schema = Joi.object().keys({
            scenarioId:Joi.number().required()
        });
        const result = schema.validate(postObj);
        return result;
    }catch(e){
        console.log("error")
        console.log(e)
    }
}
 module.exports = {validateSc, validateScenarioId}