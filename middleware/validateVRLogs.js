const Joi = require('joi');//.extend(require('@hapi/joi-date'));
const { NULL } = require('mysql2/lib/constants/types');

const validateSessionVR = Joi.object().keys({sessionId:Joi.number().required(),
                                            statement:Joi.string().default(NULL),
                                            time:Joi.number().default(NULL)});
                                    
module.exports = {validateSessionVR}