var query = require("../utility/query.js")
module.exports = {
    isAuthorization:async function (req,res,next)
    {
     
     if(req.headers.authorization)
     {
        try{
            var rows= await module.exports.getAuth(req.headers.authorization);
            if(rows.length)
            {
                let ttl = rows[0]['timeToLive'];
                let nowTimeStamp=new Date();
                if(ttl>nowTimeStamp){
                    return next();
                }     
                else{
                    res.status(401).json({
                        success: false,
                        message: 'Token is expired'
                    });
                }
            }
            else
            {
                res.status(401).json({
                    success: false,
                    message: 'Token is invalid'
                  });
            }     
        }catch(e){
            console.log("error")
            console.log(e);
        } 
     }
     else{
        res.status(401).json({
            success: false,
            message: 'You are not authorized!'
          });
     }
    },
    getAuth:async function (auth){
        try{
            var [rows] = await query(`select * from user_auth  where token="${auth}" `);
            return rows       
        }catch(e){
            console.log("error")
            console.log(e);
        }
        

    }
}