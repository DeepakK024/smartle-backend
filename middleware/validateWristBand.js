
const Joi = require('joi')//.extend(require('@hapi/joi-date'));
module.exports = {
    validateWristBand: function (postObj) {
    var schemaDesp = Joi.object().keys({
        scenarioName: Joi.string().required(),
        patientName: Joi.string().required(),
        masterManikinId: Joi.number().required(),
        dob: Joi.date().required(),
        mrn: Joi.string().required(),
        height:Joi.number().required(),
        weight: Joi.number().required(),
        hospitalName: Joi.string().required(),
        isPublic:Joi.number().required()
    });
    var wristBandsDesp = Joi.array().items({
        id:Joi.number().allow(null).default(null),
        type:Joi.number().required(),
        hand:Joi.number().allow(null).default(null),

    });
    const schema = Joi.object().keys({
            wristBands:wristBandsDesp,
            description: schemaDesp,
            scenarioId:Joi.number().required(),
    });

    const result = schema.validate(postObj);
    return result;
}
}
