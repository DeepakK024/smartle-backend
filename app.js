const express = require('express');
const app = express();
var cors = require('cors');
const dotenv = require('dotenv');
var getUserCommunication = require('./controllers/api/communication/getUserCommunication');
var postUserCommunication = require('./controllers/api/communication/postUserCommunication');
var saveSession = require('./controllers/api/session/sessionSave'); 
var login = require('./controllers/api/login/login');
var pdfVR = require('./controllers/api/misc/vrlogsPDF');

var manikin = require('./controllers/api/manikin/manikin');
var ivsites = require('./controllers/api/ivsites/getIVSites');
var saveIVSite = require('./controllers/api/ivsites/postIVSite');
var isActive = require('./controllers/api/nurses/isActive');
var allMedical = require('./controllers/api/medicalHistory/allMedical');
var labResults = require('./controllers/api/labResults/labResults');
var devicesList = require('./controllers/api/devices/getDevices');
var allergies = require('./controllers/api/allergy/patientAllergies');
var masterAllergy = require('./controllers/api/allergy/masterAllergy');
var woundsdressings = require('./controllers/api/WoundsDressings/patientWoundsDressing');
var savewoundsdressings = require('./controllers/api/WoundsDressings/postPatientWoundsDressing');
var wristBands = require('./controllers/api/wristBands/wristBands');
var savewristBands = require('./controllers/api/wristBands/postWristBand');
var scenarioList = require('./controllers/api/Scenario/scenerio');
var medicalList = require('./controllers/api/medicalHistory/medicalHistory');
var delScenario = require('./controllers/api/Scenario/deleteScenario');
var copyScenario = require('./controllers/api/Scenario/copyScenario');
var Auth = require('./middleware/authorization');
var createScenerio = require('./controllers/api/Scenario/createScenario.js');
var getUserMedicalHistory = require('./controllers/api/medicalHistory/getPatientMedicalHistory.js');
var getUserSurgicalHistory = require('./controllers/api/surgicalHistory/getPatientSurgicalHistory.js');
var createMedicalNotes = require('./controllers/api/medical_notes/createMedicalNotes.js');
var createAllergy = require('./controllers/api/allergy/createAllergy');
var forgotPassword = require('./controllers/api/user/forgotPassword.js');
var changePassword = require('./controllers/api/user/changePassword.js');
var updatePassword = require('./controllers/api/user/updatePassword.js');
var logOut =  require('./controllers/api/user/logOut.js');
var basicScenarioDetails =  require('./controllers/api/Scenario/scenarioBasicDetail');
var createLabResult =  require('./controllers/api/labResults/createLabResult');
var createDevice =  require('./controllers/api/devices/createDevices');
var startSession = require(`./controllers/api/session/sessionStart`);
var sessionEnd = require(`./controllers/api/session/sessionEnd`);
var scenerioInfoVr = require(`./controllers/api/Scenario/scenerioInfoVr`);
var updateVR = require('./controllers/api/session/updateVRLogs');
var isLastSave = require('./controllers/api/session/isLastlySaved');
var getDefaultSingle = require('./controllers/api/session/getDefault');
var postSessionComm = require('./controllers/api/session/updateSessionComm');
const devices = require('./middleware/devices');
dotenv.config();

app.use(express.json());
app.use(express.urlencoded({extended: true})); //Parse URL-encoded bodies
app.use(cors());

app.post('/login', login.login);
app.get('/manikin', Auth.isAuthorization, manikin.manikin);
app.post('/isActive', Auth.isAuthorization, isActive.isActive);
app.post('/allMedical', Auth.isAuthorization, allMedical.getMedical);
app.post('/ivsites', Auth.isAuthorization, ivsites.ivsites);
app.post('/postivsites', Auth.isAuthorization, saveIVSite.saveIVSite);
app.post('/labresults', Auth.isAuthorization, labResults.labResults);
app.post('/devicesList', Auth.isAuthorization, devicesList.devicesList);
app.post('/createLabResult', Auth.isAuthorization, createLabResult.createLabResults);
app.post('/createDevices', Auth.isAuthorization, createDevice.createDevices);
app.post('/woundsdressing', Auth.isAuthorization, woundsdressings.woundsdressings);
app.post('/postwoundsdressing', Auth.isAuthorization, savewoundsdressings.savewoundsdressings);
app.post('/wristbands', Auth.isAuthorization, wristBands.wristBands);
app.post('/postwristbands', Auth.isAuthorization, savewristBands.savewristBands);
app.post('/startSession', Auth.isAuthorization, startSession.startSession); 
app.post('/endSession', Auth.isAuthorization, sessionEnd.sessionEnd);
app.post('/saveSession', Auth.isAuthorization, saveSession.saveSession);
app.post('/createScenerio', Auth.isAuthorization,createScenerio.createScenerio);
app.post('/getUserMedicalHistory', Auth.isAuthorization,getUserMedicalHistory.getMedicalHistory);
app.post('/getUserSurgicalHistory', Auth.isAuthorization,getUserSurgicalHistory.getSurgicalHistory);
app.post('/createMedicalNotes', Auth.isAuthorization,createMedicalNotes.createMedicalNotes);
app.post('/createAllergy', Auth.isAuthorization,createAllergy.createAllergy);
app.post('/allergies',Auth.isAuthorization,allergies.patientAllergies);
app.post('/masterAllergy',Auth.isAuthorization,masterAllergy.masterAllergires);
app.get('/scenerioInfoVr',Auth.isAuthorization,scenerioInfoVr.scenerioInfoVr);
app.post('/postUserCommunication',Auth.isAuthorization,postUserCommunication.postUserCommunication);
app.post('/updatevrlogs',Auth.isAuthorization,updateVR.updateVRLogs);
app.post('/islastsave', Auth.isAuthorization, isLastSave.checkScenario);
app.post('/getDefaultObject', Auth.isAuthorization, getDefaultSingle.getDefault);
app.post('/saveComm', Auth.isAuthorization, postSessionComm.postSessionComm);
app.post('/savePDF', Auth.isAuthorization, pdfVR.vrPDF);

app.post('/forgotPassword',forgotPassword.forgotPassword);
app.post('/changePassword',changePassword.changePassword);
app.post('/updatePassword',Auth.isAuthorization,updatePassword.updatePassword);
app.get('/logOut',Auth.isAuthorization,logOut.logOut);

app.post('/scenarioList', Auth.isAuthorization, scenarioList.scenarioList);
app.get('/medicalList', Auth.isAuthorization, medicalList.medicalList);
app.post('/getCommunication', Auth.isAuthorization, getUserCommunication.getUserCommunication);
app.post('/delScenario', Auth.isAuthorization, delScenario.delScenario);
app.post('/copyScenario', Auth.isAuthorization, copyScenario.copyScenario);
app.post('/basicScenarioDetails', Auth.isAuthorization, basicScenarioDetails.basicScenarioDetails);
app.get('/', (req, res) => res.send('SmartLE App'));

//app.listen(port, () => console.log(`SmartLE app listening at http://localhost:${port}`));

require('dotenv').config();
//console.log(process.argv[2]);

module.exports = app;

