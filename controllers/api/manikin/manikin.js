const {getManikin} = require('../../../models/manikin');
module.exports = {
    manikin :  async function (req,res){
        try{
            console.log("Route Manikin hit at ",new Date().toISOString());
            const qResult = await getManikin();
            if(!qResult.length){
                var data = {
                    "manikin":[]
                }
                console.log("Manikin Responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data});
            }
            console.log("Manikin Responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result:{manikin:qResult}});
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}