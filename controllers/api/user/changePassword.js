const {getData} = require('../../../models/changePassword');
const {validateChangePassword} = require('../../../middleware/changePassword');
module.exports={
    changePassword: async function(req,res){
        try{
            console.log("Route changePassword hit at ",new Date().toISOString());
            var validateData = await validateChangePassword(req.body);
            if(!validateData.error){
                var data = await getData(req.body);
                if(data){
                    console.log("changePassword responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:true, message:'Success'});
                }
                else{
                    console.log("changePassword responding back at ",new Date().toISOString());
                    return res.status(406).send({ status:false, message:'Unsuccess'});
                }
            }
            else{
                console.log("changePassword responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }           
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}
 
 