const {getData} = require('../../../models/updatePassword');
const {validateUpdatePassword} = require('../../../middleware/updatePassword');
module.exports={
    updatePassword: async function(req,res){
        try{
            console.log("Route updatePasword hit at ",new Date().toISOString());
            var validateData = await validateUpdatePassword(req.body);
            if(!validateData.error){
                var data = await getData(req.headers,req.body);
                if(data){
                    console.log("updatePassword responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:true, message:'Success'});
                }
                else{
                    console.log("updatePassword responding back at ",new Date().toISOString());
                    return res.status(406).send({ status:false, message:'Unsuccess'});
                }
            }
            else{
                console.log("updatePassword responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }           
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}
 
 