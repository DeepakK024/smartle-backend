const {getData} = require('../../../models/forgotPassword');
const {validateForgotPassword} = require('../../../middleware/forgotPassword');
module.exports={
    forgotPassword: async function(req,res){
        try{
            console.log("Route forgotPassword hit at ",new Date().toISOString());
            var validateData = await validateForgotPassword(req.body);
            if(!validateData.error){
                var data = await getData(req.body);
                if(data==true){
                    console.log("forgotPassword responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:true, message:'Success'});
                }
                else{
                    console.log("forgotPassword responding back at ",new Date().toISOString());
                    return res.status(406).send({ status:false, message:data});
                }
            }
            else{
                console.log("forgotPassword responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }           
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}
 
 