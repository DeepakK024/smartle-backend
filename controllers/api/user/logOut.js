const {getData} = require('../../../models/logOut');
module.exports={
    logOut: async function(req,res){
        try{
            console.log("Route logout hit at ",new Date().toISOString());
            var data = await getData(req.headers);
            if(data){
                console.log("logout responding back at ",new Date().toISOString());
                return res.status(200).send({ status:true, message:'Success'});
            }
            else{
                console.log("logout responding back at ",new Date().toISOString());
                return res.status(406).send({ status:false, message:'Unsuccess'});
            }          
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}
 
 