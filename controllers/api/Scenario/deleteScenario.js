
const {deleteScenario}  = require('../../../models/deleteScenario');
const {scenarioDelSchema} = require("../../../middleware/validateschema");
const { userAuth, isUserSame } = require('../../../models/createMedicalNotes');

module.exports = {
    delScenario : async function (req,res) {
        try{
            console.log("Route deleteScenario hit at ",new Date().toISOString());
            const check = await scenarioDelSchema.validateAsync(req.body); 
            var id = await userAuth(req.headers.authorization);
            var rightUser = await isUserSame(check['scenarioId']);
            if(rightUser.length && (id[0].userId==rightUser[0].createdBy)){ 
                const results =  await deleteScenario(check);
                console.log("deleteScenario responding back at ",new Date().toISOString());
                return res.status(200).send({ status:true, message:'Success',result: "Scenario deleted scessfully"}); 
            }
            else {
                console.log("deleteScenario responding back at ",new Date().toISOString());
                return res.status(406).send({ status: false, message: "Unauthorized to delete" });
            }
        }
        catch(err){
            return res.status(406).send({ message: err.message });
        } 
    }
}