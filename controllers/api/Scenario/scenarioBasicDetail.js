
const basicDetailsQuery = require('../../../models/scenarioBasicDetail');
const {scenarioDelSchema} = require('../../../middleware/validateschema');

module.exports = {
    basicScenarioDetails : async function (req,res) {
        try{
            console.log("Route scenarioBasicDetail hit at ",new Date().toISOString());
            const checked_id = await scenarioDelSchema.validateAsync(req.body);
            const solution = await basicDetailsQuery(checked_id);
            var structured_data =  await module.exports.Structuring(solution);
            console.log("scenarioBasicDetail responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result: structured_data}); 
        }
        catch(err){
            return res.status(406).send({ message: err.message });
        }
    },
    Structuring : async function  (data) {
        const result = {
            scenarioDetail : {
                description :
                {
                    id : data[0].id,
                scenarioName : data[0].scenarioName,
                patientName : data[0].patientName,
                dob : data[0].dob,
                mrn : data[0].mrn,
                height : data[0].height,
                weight : data[0].weight,
                hospitalName: data[0].hospitalName,
                isPublic: data[0].isPublic,
                userId:data[0].userId
                },
                manikin : 
                {
                    id : data[0].manikinId,
                    age : data[0].manikinAge,
                    gender : data[0].gender,
                    ethnicity : data[0].ethnicity,
                    manikinName : data[0].name
                },
                reasonForAdmission : data[0].reasonForAdmission,
                currentStatus : data[0].currentStatus,
                otherAllergies : data[0].otherAllergies,
                recommendations : data[0].recommendations
            }
        }
        return result;
    }
}