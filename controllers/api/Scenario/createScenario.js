const {getData} = require('../../../models/createScenerio');
const {validateCreateScenerio} = require('../../../middleware/scenario');
const { userAuth } = require('../../../models/createMedicalNotes');
module.exports={
    createScenerio: async function(req,res){
        try{
            console.log("Route createScenario hit at ",new Date().toISOString());
            var id = await userAuth(req.headers.authorization);
            var scenerioData = await validateCreateScenerio(req.body);
            if(!scenerioData.error){
                var data = await getData(scenerioData['value'],id[0].userId);
                if(data){
                    console.log("createScenario responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:true, message:'Success',result:data});
                }
                else{
                    console.log("createScenario responding back at ",new Date().toISOString());
                    return res.status(406).send({ status:false, message:'Unsuccess'});
                }
            }
            else{
                console.log("createScenario responding back at ",new Date().toISOString());
                return res.status(406).send({ message: scenerioData.error.message });
            }
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}