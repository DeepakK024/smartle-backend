const {scenarioBodySchema} = require('../../../middleware/validateschema');
const {scenarioListQuery} = require('../../../models/scenerio');
const { userAuth } = require('../../../models/createMedicalNotes');

module.exports = {
scenarioList  : async function (req,res) 
{
    try{
      console.log("Route scenarioList hit at ",new Date().toISOString());
      var id = await userAuth(req.headers.authorization);
      const Scenario = await scenarioBodySchema.validateAsync(req.body.pagination);
      const ScenarioList = await scenarioListQuery(req.body, id[0].userId);
      if (!ScenarioList['scenarioList'].length){
        console.log("scenarioList responding back at ",new Date().toISOString());
        return res.status(200).send({ status:false, message:'No data found',result:ScenarioList});
      }
      console.log("scenarioList responding back at ",new Date().toISOString());
      return res.status(200).send({ status:true, message:'Success',result: ScenarioList});  
    }
    catch(err){
        console.log("error", err.message);
        return res.status(406).send({ message: err.message });
    }
}
}