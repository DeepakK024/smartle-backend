
const {scenarioCopySchema} = require('../../../middleware/validateschema');
const {Copy} = require('../../../models/copyScenario');
const { userAuth } = require('../../../models/createMedicalNotes');

module.exports = {
    copyScenario : async function (req,res){
        try{
            console.log("Route copyScenario hit at ",new Date().toISOString());
            var id = await userAuth(req.headers.authorization);
            var checked_data = await scenarioCopySchema.validateAsync(req.body);
            var results =  await Copy(checked_data, id[0].userId);
            console.log("copyScenario responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result: "Copied Successfully"}); 
        }
        catch(err){
            return res.status(406).send({ message: err.message });
        }
    }
}