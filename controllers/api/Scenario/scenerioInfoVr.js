const { getData } = require('../../../models/scenerioInfoVr');
module.exports = {
    scenerioInfoVr: async function (req, res) {
        try {
            console.log("Route for ScenarioInfo at ",new Date().toISOString());
            var data = await getData(req.headers, res);
            if (Object.keys(data).length) {
                console.log("ScenarioInfo responding back at ",new Date().toISOString());
                return res.status(200).send({ status: true, message: 'Success', data: data });
            } else {
                console.log("ScenarioInfo responding back ELSE at ",new Date().toISOString());
                return res.status(200).send({ status: false, message: "No data found", data: data });
            }
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}