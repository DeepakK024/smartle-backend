const { getDevicesList,parseInfo} = require('../../../models/devicesList');
const { validateSc } = require('../../../middleware/validateScenarioId');

module.exports = {
    devicesList: async function (req, res) {
        try {
            console.log("Route AllDevices hit at ",new Date().toISOString());
            const userData = await validateSc.validateAsync(req.body);
            const qResult = await getDevicesList(userData['scenarioId']);
            if (!qResult.length) {
                var data = {
                    "devicesList":[]
                }
                console.log("AllDevices responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data});
            }
            finalResult = await parseInfo(qResult);
            console.log("AllDevices responding back at ",new Date().toISOString());
            return res.status(200).send({ status: true, message: 'Success', result:{devicesList:qResult} });
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}