const validateCommunication = require('../../../middleware/validateCommunication');
const { Execution } = require('../../../models/postUserCommunication');
const { isSessionActive } = require('../../../models/updateVRLogs');
const { userAuth } = require('../../../models/createMedicalNotes');

module.exports = {
    postUserCommunication: async function (req, res) {
        try {
            console.log("Route postCommunication hit at ", new Date().toISOString());
            var id = await userAuth(req.headers.authorization);
            const checked = await validateCommunication.validateCommunication(req.body);
            const sessionExist = await isSessionActive(checked.sessionId);
            if (sessionExist[0].isActive == 0) {
                console.log("postCommunication responding back at with error", new Date().toISOString());
                return res.status(200).send({ status: false, message: 'Session does not exist', result: [] });
            }
            else {
                const results = await Execution(checked, id[0].userId);
                console.log("postCommunication Responding back at success ", new Date().toISOString());
                return res.status(200).send({ status: true, message: 'Success', result: results });
            }
        }
        catch (error) {
            return res.status(406).send({ message: error });
        }
    }
}