const validateSessionID = require('../../../middleware/validateSessionID');
const { Execution } = require('../../../models/getUserCommunication');
const { ExecutionM } = require("../../../models/masterCommunication");
const {isSessionActive} = require('../../../models/updateVRLogs');

module.exports = {
    getUserCommunication : async function (req,res){
        try{
            console.log("Route userCommunication hit at ",new Date().toISOString());
            const checked = await validateSessionID.validateSessionID(req.body);
                if(checked.isDefault == 0){
                    const sessionExist = await isSessionActive(checked.sessionId);
                    if(sessionExist[0].isActive==0){
                        console.log("userCommunication responding back at ",new Date().toISOString());
                        return res.status(200).send({ status:false, message:'Session does not exist',result : []});
                    }
                    else {
                        const results = await Execution(checked);
                        if(!results.length){
                            var data = {
                                "userCommunication":[]
                            }
                            console.log("userCommunication Responding back at ",new Date().toISOString());
                            return res.status(200).send({ status:false, message:'No data found',result:data});
                        }
                        else{
                            data = await module.exports.getData(results);
                        }
                    }
                }
                else {
                    const results = await ExecutionM();
                    if(!results.length){
                        var data = {
                            "userCommunication":[]
                        }
                        console.log("userCommunication Responding back at ",new Date().toISOString());
                        return res.status(406).send({ status:false, message:'No data found',result:data});
                    }
                    else {
                        data = await module.exports.getData(results);
                    } 
                }
                console.log("userCommunication Responding back at ",new Date().toISOString());
                return res.status(200).send({ status:true, message:'Success',result : {
                    userCommunication : data
                }});
        }
        catch(error){
            return res.status(406).send({ message: error });
        }
    },
    getData: async function (data){
        for(let i=0; i<data.length; i++){
            data[i]['response'] = await module.exports.getJson(data[i]['response']);
        }
         return data;
    },
    getDataM: async function (data){
        for(let i=0; i<data.length; i++){
             data[i]['response'] = await module.exports.getJson(data[i]['response']);
        }
         return data;
    },
    getJson : async function(val){
         return JSON.parse(val);
    }
}