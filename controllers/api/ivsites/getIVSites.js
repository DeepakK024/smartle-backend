const {getIVSites} = require('../../../models/ivsites');
const {validateSc} = require('../../../middleware/validateScenarioId');

module.exports = {
    ivsites : async function (req,res){
        try{
            console.log("Route IVSites hit at  ",new Date().toISOString());
            const userData = await validateSc.validateAsync(req.body);
            const qResult = await getIVSites(userData['scenarioId']);
            if(!qResult.length){
                var data = {
                    "ivSites":[]
                }
                console.log("ivSites Responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data});
            }
            console.log("ivSites Responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result:{ivSites:qResult}});
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}