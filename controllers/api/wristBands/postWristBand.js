const moment = require ('moment');
const {insertWristBand} = require('../../../models/wristBands');
const {validateWristBand} = require('../../../middleware/validateWristBand');
const { userAuth, isUserSame } = require('../../../models/createMedicalNotes');

module.exports = {
    savewristBands : async function (req,res){
        try{
            console.log("Route postWristBands hit at ",new Date().toISOString());
            const userData = await validateWristBand(req.body);
            if (!userData.error) {
                var id = await userAuth(req.headers.authorization);
                var rightUser = await isUserSame(userData['value']['scenarioId']);
                if(rightUser.length && (id[0].userId==rightUser[0].createdBy)){ 
                    const result = await insertWristBand(userData['value'], id[0].userId);
                    if (result == true) {
                        console.log("postWristBands responding back at ",new Date().toISOString());
                        return res.status(200).send({ status: true, message: 'Success' });
                    }
                    else {
                        console.log("postWristBands responding back at ",new Date().toISOString());
                        return res.status(406).send({ status: false, message: result });
                    }
                }
                else {
                    console.log("postWristBands responding back at ",new Date().toISOString());
                    return res.status(406).send({ status: false, message: "Unauthorized to edit" });
                }
            }
            else{
                console.log("postWristBands responding back at ",new Date().toISOString());
                return res.status(406).send({ message: userData.error.message });
            }
           
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    },
    getData :
    async function (data){
        let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        try {
            const wristBand=await insertWristBand(data,nowTimeStamp);
            return wristBand;
        }catch(e){
            throw e
        }
    }
}