const {getWristBands} = require('../../../models/wristBands');
const {validateSc} = require('../../../middleware/validateScenarioId');

module.exports = {
    wristBands : async function (req,res){
        try{
            console.log("Route wristBands hit at ",new Date().toISOString());
            const userData = await validateSc.validateAsync(req.body);
            const qResult = await getWristBands(userData['scenarioId']);
            if(!qResult.length ){
                var data1 = {
                    "wristBands":[]
                }
                console.log("wristBands responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data1});
            }
            console.log("wristBands responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result:{wristBands:qResult}});
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}