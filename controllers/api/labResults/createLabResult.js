const { getData } = require('../../../models/createLabTest');
const { userAuth, isUserSame } = require('../../../models/createMedicalNotes');
const { validateLabTests } = require('../../../middleware/labTest');
module.exports = {
    createLabResults: async function (req, res) {
        try {
            console.log("Route CreateLabResults hit at ",new Date().toISOString());
            var validateData = await validateLabTests(req.body);
            if (!validateData.error) {
                var id = await userAuth(req.headers.authorization);
                var rightUser = await isUserSame(validateData['value']['scenarioId']);
                if(rightUser.length && (id[0].userId==rightUser[0].createdBy)){ 
                    var data = await getData(req.body,id[0].userId);
                    if (data==true) {
                        console.log("CreateLabResults responding back at ",new Date().toISOString());
                        return res.status(200).send({ status: true, message: 'Success' });
                    }
                    else {
                        console.log("CreateLabResults responding back at ",new Date().toISOString());
                        return res.status(406).send({ status: false, message: data });
                    }
                }
                else{
                    console.log("CreateLabResults responding back at ",new Date().toISOString());
                    return res.status(406).send({ status: false, message: "Unauthorized to edit" });
                }
            }
            else {
                console.log("CreateLabResults responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }
        } catch (error) {
            console.log(error)
            return res.status(406).send({ message: error.message });
        }
    }
}