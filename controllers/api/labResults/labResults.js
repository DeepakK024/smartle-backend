const { getLabResults} = require('../../../models/labresults');
const { validateSc } = require('../../../middleware/validateScenarioId');

module.exports = {
    labResults: async function (req, res) {
        try {
            console.log("Route LabResults hit at ",new Date().toISOString());
            const userData = await validateSc.validateAsync(req.body);
            const qResult = await getLabResults(userData['scenarioId']);
            if (!qResult.length) {
                var data = {
                    "labResults":[]
                }
                console.log("LabResults responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data});
            }
            console.log("LabResults responding back at ",new Date().toISOString());
            return res.status(200).send({ status: true, message: 'Success', result:{labResults:qResult} });
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}