const {create} = require('../../../models/vrlogsPDF');

module.exports = {
    vrPDF : async function (req,res){
        try{
            console.log("Route generate PDF hit at  ",new Date().toISOString());
            const filename = await create(req.body.sessionId);
            console.log("generate PDF Responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result:"created"});
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}