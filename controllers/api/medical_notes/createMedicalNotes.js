const { getData, userAuth, isUserSame } = require('../../../models/createMedicalNotes');
const { validateMedicalNotes } = require('../../../middleware/medicalNotes');
module.exports = {
    createMedicalNotes: async function (req, res) {
        try {
            console.log("Route CreateMecialNotes hit at ",new Date().toISOString());
            var validateData = await validateMedicalNotes(req.body);
            if (!validateData.error) {
                var id = await userAuth(req.headers.authorization);
                var rightUser = await isUserSame(validateData['value']['scenarioId']);
                if(rightUser.length && (id[0].userId==rightUser[0].createdBy)){ 
                    var data = await getData(validateData['value'], id[0].userId);
                    if (data == true) {
                        console.log("CreateMedicalNotes Responding back at ",new Date().toISOString());
                        return res.status(200).send({ status: true, message: 'Success' });
                    }
                    else {
                        console.log("CreateMedicalNotes Responding back at ",new Date().toISOString());
                        return res.status(406).send({ status: false, message: data });
                    }
                }
                else {
                    console.log("CreateMedicalNotes Responding back at ",new Date().toISOString());
                    return res.status(406).send({ status: false, message: "Unauthorized to edit" });
                }
            }
            else {
                console.log("CreateMedicalNotes Responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}

