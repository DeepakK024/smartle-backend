const {getWoundsDressings} = require('../../../models/patientWoundsDressings');
const {validateSc} = require('../../../middleware/validateScenarioId');
module.exports = {
    woundsdressings : async function (req,res){
        try{
            console.log("Route woundsDressing hit at ",new Date().toISOString());
            const userData = await validateSc.validateAsync(req.body);
            const qResult = await getWoundsDressings(userData['scenarioId']);
            if(!qResult.length){
                var data1 = {
                    "woundsDressing":[]
                }
                console.log("woundsDressing responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data1});
            }
            console.log("woundsDressing responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result:{woundsDressing:qResult}});
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}