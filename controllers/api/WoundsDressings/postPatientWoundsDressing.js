const {insertWoundsDressing} = require('../../../models/patientWoundsDressings');
const { userAuth, isUserSame } = require('../../../models/createMedicalNotes');
const {validateWoundsDressing} = require('../../../middleware/validateWoundsDressing');

module.exports  = {
    savewoundsdressings : async function (req,res){
        try{
            console.log("Route postWoundsDressing hit at ",new Date().toISOString());
            const userData = await validateWoundsDressing(req.body);
            if (!userData.error) {
                var id = await userAuth(req.headers.authorization);
                var rightUser = await isUserSame(userData['value']['scenarioId']);
                if(rightUser.length && (id[0].userId==rightUser[0].createdBy)){ 
                    const result = await insertWoundsDressing(userData['value'], id[0].userId);
                    if (result == true) {
                        console.log("postWoundsDressing responding back at ",new Date().toISOString());;
                        return res.status(200).send({ status: true, message: 'Success' });
                    }
                    else {
                        console.log("postWoundsDressing responding back at ",new Date().toISOString());
                        return res.status(406).send({ status: false, message: result });
                    }
                }
                else {
                    console.log("postWoundsDressing responding back at ",new Date().toISOString());
                    return res.status(406).send({ status: false, message: "Unauthorized to edit" });
                }
            }
            else{
                console.log("postWoundsDressing responding back at ",new Date().toISOString());
                return res.status(406).send({ message: userData.error.message });
            }
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}