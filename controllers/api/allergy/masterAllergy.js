const {getMasterAllergy} = require('../../../models/masterallergy');
const {validateAllergy} = require('../../../middleware/validateAllergy');
module.exports={
    masterAllergires:async function(req, res)
    {
        try{
            console.log("Route Master Allergy hit at ",new Date().toISOString());
            const userData = await validateAllergy.validateAsync(req.body);
            const qResult = await getMasterAllergy(userData['type']);
            if(!qResult.length){
                var data = {
                    "allergies":[]
                }
                console.log("Master Allergy Responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data});
            } 
            console.log("Master Allergy Responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result:{allergies:qResult}});
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}
