const { getAllergies} = require('../../../models/patientAllergies');
const { validateAlType } = require('../../../middleware/validateAllergyType');

module.exports = {
    patientAllergies: async function (req, res) {
        try {
            console.log("Route Patient Allergy hit at ",new Date().toISOString());
            const userData = await validateAlType.validateAsync(req.body);
            const qResult = await getAllergies(userData);
            if (!qResult.length) {
                var data = {
                    "allergies":[]
                }
                console.log("Patient Allergy Responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data});
            }
            console.log("Patient Allergy Responding back at ",new Date().toISOString());
            return res.status(200).send({ status: true, message: 'Success', result:{allergies:qResult} });
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}