const {getUser, getData, endAuth} = require('../../../models/login');
const {userValid} = require('../../../middleware/userAuth');
const crypto = require('crypto');
const {insertAuth, updateActive, getAuthToken} = require('../../../models/insert');
const moment = require ('moment');
const {isSessionExisting} = require('../../../models/logOut');
var {Execution} = require('../../../models/sessionEnd');

module.exports = { 
    login : async function(req,res){
        try{
            console.log("Route Login hit at ",new Date().toISOString());
            const userData = await userValid.validateAsync(req.body);
            const qResult = await getUser(userData['user']);
            if(qResult.length === 0){
                console.log("Login responding back at ",new Date().toISOString());
                return res.status(406).send({ status:false, message:'Invalid credentials'});
            }
            var token;
            var sessionId = await isSessionExisting(qResult[0]['user_id']);
            if(sessionId.length){
                var deleteData = await Execution(sessionId[0]);
            }
            const authEnded = await endAuth(qResult[0]['user_id']);
            if(authEnded){
                token = crypto.randomBytes(16).toString('hex');
                let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
                try {
                    const auth=await insertAuth(qResult[0]['user_id'],token, nowTimeStamp);
                    await updateActive(qResult[0]['user_id']);
                }catch(e){
                    console.log("error in inserting auth ", e.message);
                }
                const result = await getData(qResult, token);
                console.log("Login responding back at ",new Date().toISOString());
                return res.status(200).send({ status:true, message:'Success',result});
            }
            else {
                console.log("Login responding back at ",new Date().toISOString());
                return res.status(406).send({ status:false, message:'Error occured on login'});
            }
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    },
    authorizedAlready: async function(ID){
        const reslt = await getAuthToken(ID);
        if(reslt.length){
            let ttl = reslt[0]['timeToLive'];
            let nowTimeStamp=new Date();
            if(ttl>nowTimeStamp){
                return reslt[0]['token'];
            }
        }
        return false;
    }
}