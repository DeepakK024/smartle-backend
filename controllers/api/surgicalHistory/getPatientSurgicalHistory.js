const { getData } = require('../../../models/getPatientSurgicalHistory');
const { validateUserSurgicalHistory } = require('../../../middleware/surgicalHistory');
module.exports = {
    getSurgicalHistory: async function (req, res) {
        try {
            console.log("Route SurgicalHistory hit at ",new Date().toISOString());
            var validateData = await validateUserSurgicalHistory(req.body);
            if (!validateData.error) {
                var data = await getData(validateData['value'], res);
                if (data[0].length) {
                    console.log("SurgicalHistory responding back at ",new Date().toISOString());
                    return res.status(200).send({
                        status: true, message: 'Success', result:{surgicalHistory: data[0]}
                    });
                }
                else {
                    var data1 = {
                        "surgicalHistory":[]
                    }
                    console.log("SurgicalHistory responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:false, message:'No data found',result:data1});
                }
            }
            else {
                console.log("SurgicalHistory responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}