
const {medicalList} = require('../../../models/medicalHistory');

module.exports = {
    medicalList :  async function(req,res){
        try{
            console.log("Route masterMeidcalHistory hit at ",new Date().toISOString());
            const data  = await medicalList();
            if (!data['data'].length){
                var data1 = {
                    "medicalList":[]
                }
                console.log("masterMedicalHistory Responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data1});
            }
            console.log("masterMedicalHistory Responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result: {
                medicalList : data
            }});  
        }
        catch(err){
            return res.status(406).send({ message: err.message });
        }
    }
}