const {activeNursesIntellisence} = require('../../../middleware/validateschema');
const {Execution} = require('../../../models/allMedical');

module.exports  = {
  getMedical : async function (req,res)
{
    try{
        console.log("Route Intellesense for medical hit at ",new Date().toISOString());
        const checked = await activeNursesIntellisence.validateAsync(req.body);
        const medical = await Execution(checked);
        console.log("Intellesense for medical Responding back at ",new Date().toISOString());
        return res.status(200).send({ status:true, message:'Success',result: {
            medicalHistory : medical
        }});  
    }
    catch(err){
        return res.status(406).send({ message: err.message });
    }
}
}