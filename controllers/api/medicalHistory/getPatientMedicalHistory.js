const { getData } = require('../../../models/getPatientMedicalHistory');
const { validateUserSurgicalHistory } = require('../../../middleware/surgicalHistory');
module.exports = {
    getMedicalHistory: async function (req, res) {
        try {
            console.log("Route patientMedicalHistory hit at ",new Date().toISOString());
            var validateData = await validateUserSurgicalHistory(req.body);
            if (!validateData.error) {
                var data = await getData(validateData['value']);
                if (data[0].length) {
                    console.log("patientMeidcalHistory Responding back at ",new Date().toISOString());
                    return res.status(200).send({
                        status: true, message: 'Success', result: {medicalHistory: data[0]}
                    });
                }
                else {
                    var data = {
                        "medicalHistory":[]
                    }
                    console.log("patientMeidcalHistory Responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:false, message:'No data found',result:data});
                }
            }
            else {
                console.log("patientMeidcalHistory Responding back at ",new Date().toISOString());
                return res.status(406).send({ message: validateData.error.message });
            }
        } catch (error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    }
}