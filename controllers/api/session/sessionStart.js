const {sessionCheckData} = require('../../../middleware/validateschema');
const {Execution, isNurseActive } = require('../../../models/sessionStart');
const {validateSessionValues} = require('../../../middleware/validateSessionValues');
module.exports = {
    startSession : async function(req,res) {
        try{
            console.log("Route sessionStart hit at ",new Date().toISOString());
            var checked;
            var data;
            if(req.body.vr === 0){
                 checked = await sessionCheckData.validateAsync(req.body);
                  data = {
                    scenarioId : req.body.scenarioId,
                    nurseId : req.body.userId,
                    trainerId : req.body.trainerId,
                    isDefault : req.body.isDefault,
                    vr : req.body.vr
                 }
                 const nurseAv = await isNurseActive(data.nurseId);
                if(nurseAv.length){
                    console.log("sessionStart responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:false, message:'Nurse Unavailable',result: []});
                }
            }
            else{
                checked = await validateSessionValues(req.body);
                 data = {
                    scenarioId : req.body['scenarioId'],
                    nurseId : req.body['userId'],
                    vr : req.body.vr
                }
                
            }
            
            const results = await Execution(data);
            if(!results){
                console.log("sessionStart responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result: []}); 
            }
            console.log("sessionStart responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result: results});  
           
        }
        catch(error){
            return res.status(406).send({ message: error });
        }
    }
}