const {validateSc} = require('../../../middleware/validateScenarioId');
const {lastSave} = require('../../../models/isLastlySaved');
module.exports = {
    checkScenario : async function(req,res) {
        try{
            console.log("Route isLastlySaved hit at ",new Date().toISOString());
            const data = await validateSc.validateAsync(req.body);
            const results = await lastSave(data);
            if(!results.length){
                console.log("isLastlySaved responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No Last saved session of this scenario',result:[]});
            }
            else {
                if(results[0].airways!=""){
                    console.log("isLastlySaved responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:true, message:'There exist session for this scenario',result:[]});
                }
                else{
                    console.log("isLastlySaved responding back at ",new Date().toISOString());
                    return res.status(200).send({ status:false, message:'No Last saved session of this scenario',result:[]});
                }
            }
        }
        catch(error){
            console.log("error ", error);
            return res.status(406).send({ message: error });
        }
    }
}