const {validateType} = require('../../../middleware/validateType');
const {getData} = require('../../../models/getDefault');
const {getJsonParsed, theWoundsDressing, theIVSites} = require('../../../models/sessionStart');

module.exports = {
    getDefault:async function(req, res){
        try{
            console.log("Route getDefault hit at ",new Date().toISOString());
            const data = await validateType(req.body);
            const results = await getData(data);
            if(!results.length){
                console.log("getDefault responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'No data found',result:data.type});
            }
            else {
                if(data.type=="" || data.type==" " || data.type==null){
                    console.log("getDefault responding back at ",new Date().toISOString());
                    return res.status(200).send({ status: false, message: 'Invalid Type', result:[] });
                }
                else {
                    const objectFromMaster = getJsonParsed(results[0], data.type);
                    if(data.type=="exposure"){
                        var ivSites = await theIVSites(data.scenarioId);
                        objectFromMaster.ivSites=ivSites[0];
                        var woundsDressing = await theWoundsDressing(data.scenarioId);
                        objectFromMaster.woundsDressing = woundsDressing[0];
                    }
                    console.log("getDefault responding back at ",new Date().toISOString());
                    return res.status(200).send({ status: true, message: 'Success', result:{objectFromMaster} });
                }
            }
        }catch(e){
            console.log("error ", e);
            return res.status(200).send({ status:false, message:e.message});
        }
        
    }
}