const {Execution} = require('../../../models/sessionEnd');
const {sessionEndCheck} = require(`../../../middleware/validateschema`);
module.exports = {
    sessionEnd : async function (req,res)
    {
        try{
            console.log("Route sessionEnd hit at ",new Date().toISOString());
            const checked = await sessionEndCheck.validateAsync(req.body);
            const results = await Execution(checked);
            console.log("sessionEnd responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result : "Session Ended Sucessfully"});
        }
        catch(err){
            return res.status(406).send({ status:false, message: err });
        }
    }
}