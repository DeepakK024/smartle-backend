const moment = require ('moment');
const {insertUpdateVR, isSessionActive} = require('../../../models/updateVRLogs');
const {validateSessionVR} = require('../../../middleware/validateVRLogs');
const { userAuth } = require('../../../models/createMedicalNotes');

module.exports  = {
    updateVRLogs : async function (req,res){
        try{
            console.log("Route updateVRLogs hit at ",new Date().toISOString());
            var id = await userAuth(req.headers.authorization);
            let result;
            const userData = await validateSessionVR.validateAsync(req.body);
            const isActive = await isSessionActive(userData['sessionId']);
            if(isActive[0]['isActive'] === 1){
                result = await module.exports.getData(userData, id[0].userId);
                console.log("updateVRLogs responding back at ",new Date().toISOString());
                return res.status(200).send({ status:true, message:'Success',result});
            }
            else {
                console.log("updateVRLogs responding back at ",new Date().toISOString());
                return res.status(200).send({ message: "Session does not exist" });
            }
            
        }catch(error) {
            console.log(error);
            return res.status(406).send({ message: error.message });
        }
    },
    getData : async function (data, userId){
        let nowTimeStamp=moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        try {
            const vrLogs=await insertUpdateVR(data,nowTimeStamp, userId);
            return vrLogs;
        }catch(e){
            throw e
        }
    }
}