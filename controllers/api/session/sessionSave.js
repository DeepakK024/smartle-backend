const {sessionSaveCheck} = require('../../../middleware/validateschema');
const {Execution } = require('../../../models/sessionSave');
const {isSessionActive} = require('../../../models/updateVRLogs');
module.exports = {
    saveSession : async function(req,res) {
        try{
            console.log("Route sessionSave hit at ",new Date().toISOString());
            const checked = await sessionSaveCheck.validateAsync(req.body);
            const sessionExist = await isSessionActive(checked.sessionId);
            if(sessionExist[0].isActive==0){
                console.log("sessionSave responding back at ",new Date().toISOString());
                return res.status(200).send({ status:false, message:'Session does not exist',result : []});
            }
            else {
                const results = await Execution(checked);
                console.log("sessionSave responding back at ",new Date().toISOString());
                return res.status(200).send({ status:true, message:'Success',result : results});
            }
        }
        catch(error){
            return res.status(406).send({ message: error });
        }
    }
}