
const {activeNursesIntellisence} = require('../../../middleware/validateschema');
const {Execution} = require('../../../models/isActive');

module.exports  = {
    isActive : async function (req,res){
        try{
            console.log("Route Active Nurses hit at ",new Date().toISOString());
            const checked = await activeNursesIntellisence.validateAsync(req.body);
            const nurses = await Execution(checked);
            console.log("ActiveNurses Responding back at ",new Date().toISOString());
            return res.status(200).send({ status:true, message:'Success',result: {
                nurses : nurses
            }});  
        }
        catch(err){
            return res.status(406).send({ message: err.message });
        }
    }
}