// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/updatevrlogs');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// require('./24getDefault.test');
// const expect=chai.expect;
// const {getToken, getSessionId} = require('../../models/getToken')

// describe('Testing Update VR Logs:',()=>{
//     var result ;
//     var token ;
//     var sessionId ;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         sessionId = await getSessionId(userId)
//         const postObject={
//             "sessionId":sessionId[0].id,
//             "statement":"Performed full operation",
//             "time":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//     })

//     it('subtest-3:should return status 406 ',async ()=>{
        
//         const postObject={
//             "statement":"Performed full operation",
//             "time":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionIds":222,
//             "statement":"Performed full operation",
//             "time":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId":222,
//             "statements":"Performed full operation",
//             "time":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId":222,
//             "statement":"Performed full operation",
//             "times":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId":"222a",
//             "statement":"Performed full operation",
//             "time":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId":222,
//             "statement":1,
//             "time":200
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId":222,
//             "statement":"Performed full operation",
//             "time":"200a"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
// })