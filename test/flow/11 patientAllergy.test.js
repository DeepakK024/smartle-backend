// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/allergies');
// const asserttype = require('chai-asserttype');
// require('./10createAllergy.test');
// chai.use(asserttype);
// const expect=chai.expect;
// const {getToken, getScenarioId} = require('../../models/getToken')

// describe('Testing Patient Allergies:',()=>{
//     var result ;
//     var token ;
//     var scId;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         scId = await getScenarioId();
//         const postObject={
//             "scenarioId":46,
//             "type" : 0
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-2:should return status 200 with response status be false ',async ()=>{
        
//         const postObject={
//             "scenarioId":999,
//             "type" : 1
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(false);
//     })

//     it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":"as",
//             "type" : 1
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 with validation error for type',async ()=>{
        
//         const postObject={
//             "scenarioIds":46
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-5:should return status 406 with validation error for scenarioId and type',async ()=>{
        
//         const postObject={}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "type":0
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

    
// })