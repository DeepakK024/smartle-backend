'use strict'
const supertest = require('supertest');
const chai = require('chai');
require('./18wristBands.test');
const api = supertest('http://localhost:5000');
const asserttype = require('chai-asserttype');
chai.use(asserttype);
const expect=chai.expect;
const {getToken, getScenarioId, deleteOtherData, getSessionId, deleteMedicalData,deleteSurgicalData,deleteAllergiesData,deleteLabData,deleteIVData,deleteWoundsData,deleteWristData, deleteScenarioData} = require('../../models/getToken')


var result ;
var token ;
var scId;
const userId = 14;
var sessionId;

describe('Testing Create Scenario:',()=>{
    // var result ;
    // var token ;
    // const userId = 14;
    it('subtest-1:should return status 200 with data',async ()=>{
        result = await getToken(userId);
        token = result[0].token;
        const postObject={
            "scenarioName":"test101",
            "patientName":"Deepak kumar",
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.number();
    })

    it('subtest-2:should return status 406 ',async ()=>{
        
        const postObject={
            "scenarioName":"A new Scenario",
            "patientName":"Deepak kumar",
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"A new Scenario",
            "patientName":"Deepak kumar",
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"A new Scenario",
            "patientName":"Deepak kumar",
            "masterManikinId":1,
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"A new Scenario",
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"A new Scenario",
            "patientName":"Deepak kumar",
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-7:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "patientName":"Deepak kumar",
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-8:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":1,
            "patientName":"Deepak kumar",
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-9:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"as",
            "patientName":1,
            "masterManikinId":1,
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-10:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"as",
            "patientName":"as",
            "masterManikinId":"1a",
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-11:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"as",
            "patientName":"as",
            "masterManikinId":"1",
            "dob":"2asa",
            "mrn": "2232",
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-12:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"as",
            "patientName":"as",
            "masterManikinId":"1",
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": 2232,
            "hospitalName":"Paras"
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-13:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"as",
            "patientName":"as",
            "masterManikinId":"1",
            "dob":"2020-09-10T07:18:08.457Z",
            "mrn": "2232",
            "hospitalName":1
        }
        const res=await api.post('/createscenerio')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //masters allergy
    it('subtest-14:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "type":1
        }
        const res=await api.post('/masterAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-15:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "type":999
        }
        const res=await api.post('/masterAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-16:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "type":"as"
        }
        const res=await api.post('/masterAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "types":46
        }
        const res=await api.post('/masterAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-18:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/masterAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    //master medical
    //copy 
    it('subtest-19:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        scId = await getScenarioId();
        const postObject={
            "scenarioId" : scId[0].id,
            "scenarioName":"test102"
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.string();
    })

    it('subtest-20:should return status 406 ',async ()=>{
        
        const postObject={
            "scenarioId" : "46a",
            "scenarioName":"Hello Honey Bunny"
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-21:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId" : 46,
            "scenarioName":1
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-22:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioName":"Hello Honey Bunny"
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-23:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId" : 46
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-24:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-25:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds" : 46,
            "scenarioName":"Hello Honey Bunny"
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-26:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId" : 46,
            "scenarioNames":"Hello Honey Bunny"
        }
        const res=await api.post('/copyscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    // basic detail 

    it('subtest-27:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        //console.log("scid in basic detail ", scId)
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/basicScenarioDetails')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-28:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/basicScenarioDetails')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-29:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/basicScenarioDetails')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-30:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/basicScenarioDetails')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-31:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/basicScenarioDetails')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    //medical notes
    it('subtest-32:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        // console.log("scId ",scId)
        const postObject={
            "scenarioId":scId[0].id,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.message).to.be.string();
    })

    it('subtest-33:should return status 406 ',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-34:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-35:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-36:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-37:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-38:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-39:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-40:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":1,
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-41:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":1,
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-42:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":"a1",
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-43:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"asasa",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-44:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": 2232,
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-45:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":1
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-46:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":3,
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-47:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
            "currentStatus":2,
            "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-48:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
            "currentStatus":" holy ",
            "recommendations":1,
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-49:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
             "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })


    it('subtest-50:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-51:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
            "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-52:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"46a",
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real",
                "masterManikinId":1,
                "dob":"2020-09-10 00:00:00",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "reasonForAdmission":"this is insane",
                "currentStatus":" holy ",
                "recommendations":"unattained",
           
            "medicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","medicalHistoryId":1},
                {"id":84,"date":"2020-09-10 00:00:00","type":"deleted","medicalHistoryId":1},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","medicalHistoryId":1}
                ],
            "surgicalHistory":[
                {"id":1,"date":"2020-09-10 00:00:00","type":"add","description":"Kidney transplant"},
                {"id":84,"date":"2020-09-10 00:00:00","type":"","description":"abc"},
                {"id":3,"date":"2020-09-10 00:00:00","type":"","description":"abc"}
                ]
        }
        const res=await api.post('/createmedicalnotes')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })



    //// Create Allergy

    it('subtest-53:should return status 200 with data',async ()=>{
        const postObject={
            "scenarioId":scId[0].id,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.message).to.be.string();
    })

    //user surgical history
    it('subtest-54:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/getusersurgicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-55:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/getusersurgicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-56:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/getusersurgicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-57:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/getusersurgicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-58:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/getusersurgicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //get medical history
    it('subtest-59:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        // console.log("scId ", scId)
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/getusermedicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-60:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/getusermedicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-61:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/getusermedicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-62:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/getusermedicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-63:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/getusermedicalhistory')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //create allergy

    it('subtest-64:should return status 406 ',async ()=>{
        
        const postObject=
            {
                "description":{
                    "scenarioName":"test101",
                    "patientName":"sarmad abbasi real part2",
                    "masterManikinId":1,
                    "dob":"2020-09-10T07:18:08.457Z",
                    "mrn": "2232",
                    "hospitalName":"Paras"
                    
                },
                "otherAllergy":"abc",
                "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                    {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                    {"id":1,"reaction":0,"type":"add","allergyId":10}]
            }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-65:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"46a",
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-66:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-67:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-68:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-69:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-70:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-71:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-72:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232"
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-73:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":1,
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-74:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":1,
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-75:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":"1a",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-76:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-78:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": 2232,
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-79:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":1
                
            },
            "otherAllergy":"abc",
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-80:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":1,
            "allergies":[{"id":1,"type":"add","allergyId":9,"reaction":0},
                {"id":1,"reaction":1,"type":"deleted","allergyId":9},
                {"id":1,"reaction":0,"type":"add","allergyId":10}]
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-81:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "otherAllergy":"abc"
        }
        const res=await api.post('/createAllergy')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    //patient allergies

    it('subtest-82:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId":scId[0].id,
            "type" : 0
        }
        const res=await api.post('/allergies')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-83:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999,
            "type" : 1
        }
        const res=await api.post('/allergies')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-84:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as",
            "type" : 1
        }
        const res=await api.post('/allergies')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-85:should return status 406 with validation error for type',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/allergies')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-86:should return status 406 with validation error for scenarioId and type',async ()=>{
        
        const postObject={}
        const res=await api.post('/allergies')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-87:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "type":0
        }
        const res=await api.post('/allergies')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //lab tests

    it('subtest-88:should return status 200 with data',async ()=>{
        const postObject={
            "scenarioId":scId[0].id,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.message).to.be.string();
    })

    it('subtest-89:should return status 406 ',async ()=>{
        
        const postObject=
            {
                "description":{
                    "scenarioName":"test101",
                    "patientName":"sarmad abbasi real part2",
                    "masterManikinId":1,
                    "dob":"2020-09-10T07:18:08.457Z",
                    "mrn": "2232",
                    "hospitalName":"Paras"
                    
                },
                "type": 0,
                "labTest": [
                    {
                        "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                        "labTestName": "hgb",
                        "date": "2020-10-17T08:32:45.000Z",
                        "value": "2"
                    }
                ]
            }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-90:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"46a",
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-91:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-92:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-93:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-94:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-95:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-96:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-97:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232"
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-98:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":1,
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type":0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-99:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":1,
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-100:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":"1a",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-101:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-102:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": 2232,
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-103:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":1
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-104:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 99,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-105:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-106:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": "0a",
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-107:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-108:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": 1,
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-109:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": 1,
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-110:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "Z",
                    "value": "2"
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-111:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "type": 0,
            "labTest": [
                {
                    "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
                    "labTestName": "hgb",
                    "date": "2020-10-17T08:32:45.000Z",
                    "value": 2
                }
            ]
        }
        const res=await api.post('/createlabresult')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //patient lab results

    it('subtest-112:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/labresults')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-113:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/labresults')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-114:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/labresults')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-115:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/labresults')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-116:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/labresults')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //IV SITES

    it('subtest-117:should return status 200 with data',async ()=>{
        const postObject={
            "scenarioId":scId[0].id,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.message).to.be.string();
    })

    it('subtest-118:should return status 406 ',async ()=>{
        
        const postObject=
            {
                "description":{
                    "scenarioName":"test101",
                    "patientName":"sarmad abbasi real part2",
                    "masterManikinId":1,
                    "dob":"2020-09-10T07:18:08.457Z",
                    "mrn": "2232",
                    "hospitalName":"Paras"
                    
                },
                "ivSites":[
                    {
                        "location":"Arm",
                        "appearance":"Death",
                        "id":null
                    },
                    {
                        "location":"Legs",
                        "appearance":"Normal",
                        "id":null
                    }
                ]
            }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-119:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"46a",
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-120:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-121:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-122:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-123:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-124:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-125:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-126:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232"
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-127:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":1,
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-128:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":1,
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-129:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":"1a",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-130:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-131:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": 2232,
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-132:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":1
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-133:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            }
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-134:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":1,
                    "appearance":"Death",
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-135:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "ivSites":[
                {
                    "location":"Arm",
                    "appearance":1,
                    "id":null
                },
                {
                    "location":"Legs",
                    "appearance":"Normal",
                    "id":null
                }
            ]
        }
        const res=await api.post('/postivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    //patient iv sites
    it('subtest-136:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/ivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-137:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/ivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-138:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/ivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-139:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/ivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-140:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/ivsites')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    ///Wounds Dressing

    it('subtest-141:should return status 200 with data',async ()=>{
        const postObject={
            "scenarioId":scId[0].id,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.message).to.be.string();
    })

    it('subtest-142:should return status 406 ',async ()=>{
        
        const postObject=
            {
                "description":{
                    "scenarioName":"test101",
                    "patientName":"sarmad abbasi real part2",
                    "masterManikinId":1,
                    "dob":"2020-09-10T07:18:08.457Z",
                    "mrn": "2232",
                    "hospitalName":"Paras"
                    
                },
                "woundsDressing":[
                    {
                        "id":null,
                        "type":0,
                        "location":"Head",
                        "appearance":"3"
                    },
                    {
                        "id":null,
                        "type":0,
                        "location":"Head",
                        "appearance":"3"
                    }
                ]
            }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-143:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"46a",
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-144:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-145:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-146:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-147:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-148:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-149:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-150:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232"
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-151:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":1,
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-152:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":1,
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-153:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":"1a",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-154:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-155:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": 2232,
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-156:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":1
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-157:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
            }
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-158:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":0,
                    "location":1,
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-159:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "woundsDressing":[
                {
                    "id":null,
                    "type":"0a",
                    "location":"Head",
                    "appearance":"3"
                },
                {
                    "id":null,
                    "type":0,
                    "location":"Head",
                    "appearance":"3"
                }
            ]
        }
        const res=await api.post('/postwoundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    //patient wounds dressing
    it('subtest-160:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/woundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-161:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/woundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-162:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/woundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-163:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":47
        }
        const res=await api.post('/woundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-164:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/woundsdressing')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //WristBands


    it('subtest-165:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId":scId[0].id,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.message).to.be.string();
    })

    it('subtest-166:should return status 406 ',async ()=>{
        
        const postObject=
            {
                "description":{
                    "scenarioName":"test101",
                    "patientName":"sarmad abbasi real part2",
                    "masterManikinId":1,
                    "dob":"2020-09-10T07:18:08.457Z",
                    "mrn": "2232",
                    "hospitalName":"Paras"
                    
                },
                "wristBands":[
                    {
                        "id":null,
                        "type":1,
                        "hand":1
                    },
                    {
                        "id":null,
                        "type":1,
                        "hand":1
                    }
                ]
            }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-167:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"46a",
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-168:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-169:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-170:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-171:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-172:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-173:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-174:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232"
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-175:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":1,
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-176:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":1,
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-177:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":"1a",
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-178:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-179:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": 2232,
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-180:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":1
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-181:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
            }
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-182:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":"1a",
                    "hand":1
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-183:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":46,
            "description":{
                "scenarioName":"test101",
                "patientName":"sarmad abbasi real part2",
                "masterManikinId":1,
                "dob":"2020-09-10T07:18:08.457Z",
                "mrn": "2232",
                "hospitalName":"Paras"
                
            },
            "wristBands":[
                {
                    "id":null,
                    "type":1,
                    "hand":"1a"
                },
                {
                    "id":null,
                    "type":1,
                    "hand":1
                }
            ]
        }
        const res=await api.post('/postwristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    //patient wrist bands
    it('subtest-185:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":scId[0].id
        }
        const res=await api.post('/wristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-186:should return status 200 with response status be false ',async ()=>{
        
        const postObject={
            "scenarioId":999
        }
        const res=await api.post('/wristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-187:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioId":"as"
        }
        const res=await api.post('/wristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-188:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={
            "scenarioIds":46
        }
        const res=await api.post('/wristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-189:should return status 406 with validation error for scenarioId',async ()=>{
        
        const postObject={}
        const res=await api.post('/wristbands')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //scenarioList
    it('subtest-286:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={"pagination" :
        {
            "limit" : "10",
            "offset": 0
        },
        "filter" :
        {
            "scenarioName" : "t",
            "patientName" : "",
            "dob" : "",
            "mrn" : "",
            "manikinName" : "",
            "ethnicity" : ""
        }}
        const res=await api.post('/scenariolist')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-288:should return status 200 with data',async ()=>{
        const postObject={"pagination" :
        {
            "limit" : 10,
            "offset": "0"
        },
        "filter" :
        {
            "scenarioName" : "t",
            "patientName" : "",
            "dob" : "",
            "mrn" : "",
            "manikinName" : "",
            "ethnicity" : ""
        }}
        const res=await api.post('/scenariolist')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-289:should return status 200 with data',async ()=>{
        const postObject={"pagination" :
        {
            "limit" : "10",
            "offset": "0"
        },
        "filter" :
        {
            "scenarioName" : "t",
            "patientName" : "",
            "dob" : "",
            "mrn" : "",
            "manikinName" : "",
            "ethnicity" : ""
        }}
        const res=await api.post('/scenariolist')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-290:should return status 200 with data',async ()=>{
        const postObject={"pagination" :
        {
            "limit" : 10,
            "offset": 0
        },
        "filter" :
        {
            "scenarioName" : "",
            "patientName" : "",
            "dob" : "",
            "mrn" : "",
            "manikinName" : "",
            "ethnicity" : ""
        }}
        const res=await api.post('/scenariolist')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
    })

    it('subtest-291:should return status 406 with validation error for limit',async ()=>{
        
        const postObject={"limit" : 10,
            "offset": 0,
        "filter" :
        {
            "scenarioName" : "",
            "patientName" : "",
            "dob" : "",
            "mrn" : "",
            "manikinName" : "",
            "ethnicity" : ""
            }
        }
        const res=await api.post('/scenariolist')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //startSession

    it('subtest-190:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId": scId[0].id,
            "trainerId":14,
            "userId": 3,
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-191:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "trainerId":14,
            "userId": 6,
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-192:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "userId": 6,
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-193:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":14,
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-194:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":14,
            "userId": 6,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-195:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":14,
            "userId": 6,
            "isDefault":1,
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-196:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": "46a",
            "trainerId":14,
            "userId": 6,
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-197:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":"14a",
            "userId": 6,
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-198:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":14,
            "userId":"6a",
            "isDefault":1,
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-199:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":14,
            "userId": 6,
            "isDefault":"1a",
            "vr": 0
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-200:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "scenarioId": 46,
            "trainerId":14,
            "userId": 6,
            "isDefault":1,
            "vr": "0a"
        }
        const res=await api.post('/startSession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //isActive

    it('subtest-202:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "search" : ""
        }
        const res=await api.post('/isActive')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-203:should return status 406 ',async ()=>{
        
        const postObject={
        }
        const res=await api.post('/isActive')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //isLastSave
    it('subtest-204:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":39
        }
        const res=await api.post('/islastsave')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-205:should return status 406 ',async ()=>{
        
        const postObject={
        }
        const res=await api.post('/islastsave')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-205:should return status 406 ',async ()=>{
        
        const postObject={
            "scenarioId":"39a"
        }
        const res=await api.post('/islastsave')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-206:should return status 406 ',async ()=>{
        
        const postObject={
            "scenarioIds":"39a"
        }
        const res=await api.post('/islastsave')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //getComm

    it('subtest-207:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "sessionId" : 224,
            "isDefault":1
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-208:should return status 406 ',async ()=>{
        
        const postObject={}
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-209:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "isDefault":0
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-210:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId" : 222
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-211:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionIds" : 222,
            "isDefault":0
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-212:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId" : 222,
            "isDefaults":0
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-213:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId" : "222a",
            "isDefault":0
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-214:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId" : 222,
            "isDefault":"a0"
        }
        const res=await api.post('/getCommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //getDefaultObject

    // it('subtest-215:should return status 200 with data',async ()=>{
    //     // result = await getToken(userId);
    //     // token = result[0].token;
    //     // scId = await getScenarioId()
    //     const postObject={
    //         "scenarioId":scId[0].id,
    //         "type":"airways"
    //     }
    //     const res=await api.post('getdefaultobject')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .timeout()
    //     .send(postObject).expect(200);
    //     console.log("res ",res)
    //     const jsonRes=JSON.parse(res.text);
    //     expect(jsonRes.status).to.be.equal(true);
    // })

    it('subtest-216:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"exposure"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-217:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"deficit"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-218:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"vitalSigns"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-219:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"miscellaneous"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-220:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"breathing"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-221:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"circulation"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-222:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"genitourinary"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-223:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"gastrointestinal"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-224:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const postObject={
            "scenarioId":1,
            "type":"composition"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-225:should return status 406 ',async ()=>{
        
        const postObject={}
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-226:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "type":"exposure"
        }
        const res=await api.post('/getdefaultobject')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    //update VR logs

    it('subtest-227:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        sessionId = await getSessionId(userId)
        const postObject={
            "sessionId":sessionId[0].id,
            "statement":"Performed full operation",
            "time":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-228:should return status 406 ',async ()=>{
        
        const postObject={
            "statement":"Performed full operation",
            "time":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-229:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionIds":222,
            "statement":"Performed full operation",
            "time":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-230:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId":222,
            "statements":"Performed full operation",
            "time":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-231:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId":222,
            "statement":"Performed full operation",
            "times":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-232:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId":"222a",
            "statement":"Performed full operation",
            "time":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-234:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId":222,
            "statement":1,
            "time":200
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })
    it('subtest-235:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId":222,
            "statement":"Performed full operation",
            "time":"200a"
        }
        const res=await api.post('/updatevrlogs')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })



    //save Session

    it('subtest-236:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // sessionId = await getSessionId(userId)
        const postObject={
            "sessionId":sessionId[0].id,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-237:should return status 406 ',async ()=>{
        
        const postObject={}
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-238:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-239:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-240:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-241:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-242:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-243:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-244:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "genitourinary" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-245:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "gastrointestinal" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-246:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "composition" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-247:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId":224,
            "airways" : "",
            "exposure" : "",
            "deficit" : "",
            "vitalSigns" : "",
            "miscellaneous" : "",
            "breathing" :  "",
            "circulation" : "",
            "genitourinary" : "",
            "gastrointestinal" : ""
        }
        const res=await api.post('/savesession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //Scenario Info VR

    it('subtest-201:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // console.log("token ",token)
        const res=await api.get('/scenerioInfoVr')
        .set('Authorization', 'b59c16063c6a1dd34fc8d9b7a9992346')
        .set('userId', 3)
        .expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        //expect(jsonRes.result).to.be.object();
    })

    //session Communication

    it('subtest-248:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // sessionId = await getSessionId(userId)
        const postObject={
            "sessionId" : sessionId[0].id,
            "communicationData" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/saveComm')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-249:should return status 406 ',async ()=>{
        
        const postObject={
            "communicationData" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/saveComm')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-250:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId" : 226
          }
        const res=await api.post('/saveComm')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-251:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId" : "226a",
            "communicationData" : 
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
          }
        const res=await api.post('/saveComm')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-252:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId" : 226,
            "communicationDatas" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/saveComm')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-253:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionIds" : 226,
            "communicationData" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/saveComm')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //save communication of user
    it('subtest-254:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // sessionId = await getSessionId(userId)
        const postObject={
            "sessionId" : sessionId[0].id,
            "communicationData" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/postusercommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-255:should return status 406 ',async ()=>{
        
        const postObject={
            "communicationData" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/postusercommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-256:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId" : 226
          }
        const res=await api.post('/postusercommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-257:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId" : "226a",
            "communicationData" : 
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
          }
        const res=await api.post('/postusercommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-258:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionId" : 226,
            "communicationDatas" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/postusercommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-259:should return status 406 ',async ()=>{
        
        const postObject={
            "sessionIds" : 226,
            "communicationData" : [
                {
                    "response":{
                      "options" : [
                              {
                              "opt" : "1/2 a pack a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "A pack a day",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Two packs a day",
                              "isDefault" : false
                              },
                              {
                              "opt" : "Only when I drink",
                              "isDefault" : false         
                              },
                              {
                              "opt" : "Only when I socialize",
                              "isDefault" : false         
                              }
                          ],
                          "selected" : {
                              "opt" : "I dont know anything"
                          }
                      },
                      "masterCommunicationId" : 4,
                      "userID" : 3
                }
            ]
          }
        const res=await api.post('/postusercommunication')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //end session

    it('subtest-260:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // sessionId = await getSessionId(userId)
        const postObject={
            "sessionId"  : sessionId[0].id,
        }
        const res=await api.post('/endsession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })

    it('subtest-261:should return status 406 ',async ()=>{
        
        const postObject={}
        const res=await api.post('/endsession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-262:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionIds" : 222
        }
        const res=await api.post('/endsession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-263:should return status 406 ',async ()=>{
        
        const postObject=
        {
            "sessionId" : "222a"
        }
        const res=await api.post('/endsession')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //delete data and scenario
    it('subtest-264:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        // scId = await getScenarioId();
        const postObject={
            "scenarioId" : scId[0].id
        }
        const res=await api.post('/delscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.string();
        await deleteMedicalData(scId[0].id);
        await deleteSurgicalData(scId[0].id);
        await deleteAllergiesData(scId[0].id);
        await deleteLabData(scId[0].id);
        await deleteIVData(scId[0].id);
        await deleteWoundsData(scId[0].id);
        await deleteWristData(scId[0].id);
        await deleteScenarioData(scId[0].id);
        await deleteOtherData(scId[0].id);

    })

    it('subtest-265:should return status 406 ',async ()=>{
        
        const postObject={
            "scenarioId" : "46a"
        }
        const res=await api.post('/delscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    it('subtest-266:should return status 406 ',async ()=>{
        
        const postObject={
        }
        const res=await api.post('/delscenario')
        .set('Authorization', token)
        .set('userId', userId)
        .send(postObject).expect(406);
    })

    //logout 
    it('subtest-267:should return status 200 with data',async ()=>{
        // result = await getToken(userId);
        // token = result[0].token;
        const res=await api.get('/logout')
        .set('Authorization', token)
        .set('userId', userId)
        .expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
    })
})















































































