// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/postwristbands');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// require('./18wristBands.test');
// const expect=chai.expect;
// const {getToken, getScenarioId} = require('../../models/getToken')

// describe('Testing Create Patient Wrist Bands:',()=>{
//     var result ;
//     var token ;
//     var scId ;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         scId = await getScenarioId();
//         const postObject={
//             "scenarioId":scId[0].id,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.message).to.be.string();
//     })

//     it('subtest-2:should return status 406 ',async ()=>{
        
//         const postObject=
//             {
//                 "description":{
//                     "scenarioName":"liver transplant",
//                     "patientName":"sarmad abbasi real part2",
//                     "masterManikinId":1,
//                     "dob":"2020-09-10T07:18:08.457Z",
//                     "mrn": "2232",
//                     "hospitalName":"Paras"
                    
//                 },
//                 "wristBands":[
//                     {
//                         "id":null,
//                         "type":1,
//                         "hand":1
//                     },
//                     {
//                         "id":null,
//                         "type":1,
//                         "hand":1
//                     }
//                 ]
//             }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":"46a",
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-7:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-8:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-9:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-10:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232"
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-11:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":1,
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-12:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":1,
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-13:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":"1a",
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-14:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-15:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": 2232,
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-16:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":1
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
//             }
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":"1a",
//                     "hand":1
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
//     it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "wristBands":[
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":"1a"
//                 },
//                 {
//                     "id":null,
//                     "type":1,
//                     "hand":1
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
// })