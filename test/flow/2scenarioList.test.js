// 'use strict'
// const supertest = require('supertest');
// require('./1login.test');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/scenariolist');
// const asserttype = require('chai-asserttype');
// const { use } = require('chai');
// chai.use(asserttype);
// const expect=chai.expect;
// const {getToken} = require('../../models/getToken')


// describe('Testing Scenario List:',()=>{
//     var result ;
//     var token ;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         const postObject={"pagination" :
//         {
//             "limit" : "10",
//             "offset": 0
//         },
//         "filter" :
//         {
//             "scenarioName" : "K",
//             "patientName" : "",
//             "dob" : "",
//             "mrn" : "",
//             "manikinName" : "",
//             "ethnicity" : ""
//         }}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-2:should return status 200 with data',async ()=>{
//         const postObject={"pagination" :
//         {
//             "limit" : 10,
//             "offset": 0
//         },
//         "filter" :
//         {
//             "scenarioName" : "K",
//             "patientName" : "",
//             "dob" : "",
//             "mrn" : "",
//             "manikinName" : "",
//             "ethnicity" : ""
//         }}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-3:should return status 200 with data',async ()=>{
//         const postObject={"pagination" :
//         {
//             "limit" : 10,
//             "offset": "0"
//         },
//         "filter" :
//         {
//             "scenarioName" : "K",
//             "patientName" : "",
//             "dob" : "",
//             "mrn" : "",
//             "manikinName" : "",
//             "ethnicity" : ""
//         }}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-4:should return status 200 with data',async ()=>{
//         const postObject={"pagination" :
//         {
//             "limit" : "10",
//             "offset": "0"
//         },
//         "filter" :
//         {
//             "scenarioName" : "K",
//             "patientName" : "",
//             "dob" : "",
//             "mrn" : "",
//             "manikinName" : "",
//             "ethnicity" : ""
//         }}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-5:should return status 200 with data',async ()=>{
//         const postObject={"pagination" :
//         {
//             "limit" : 10,
//             "offset": 0
//         },
//         "filter" :
//         {
//             "scenarioName" : "",
//             "patientName" : "",
//             "dob" : "",
//             "mrn" : "",
//             "manikinName" : "",
//             "ethnicity" : ""
//         }}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-6:should return status 406 with validation error for limit',async ()=>{
        
//         const postObject={"limit" : 10,
//             "offset": 0,
//         "filter" :
//         {
//             "scenarioName" : "",
//             "patientName" : "",
//             "dob" : "",
//             "mrn" : "",
//             "manikinName" : "",
//             "ethnicity" : ""
//             }
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
// })