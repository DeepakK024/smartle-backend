// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/createlabresult');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// require('./12labresults.test');
// const expect=chai.expect;
// const {getToken, getScenarioId} = require('../../models/getToken')

// describe('Testing Create Patient Lab Results:',()=>{
//     var result ;
//     var token ;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         scId = await getScenarioId();
//         const postObject={
//             "scenarioId":scId[0].id,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.message).to.be.string();
//     })

//     it('subtest-2:should return status 406 ',async ()=>{
        
//         const postObject=
//             {
//                 "description":{
//                     "scenarioName":"liver transplant",
//                     "patientName":"sarmad abbasi real part2",
//                     "masterManikinId":1,
//                     "dob":"2020-09-10T07:18:08.457Z",
//                     "mrn": "2232",
//                     "hospitalName":"Paras"
                    
//                 },
//                 "type": 0,
//                 "labTest": [
//                     {
//                         "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                         "labTestName": "hgb",
//                         "date": "2020-10-17T08:32:45.000Z",
//                         "value": "2"
//                     }
//                 ]
//             }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":"46a",
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-7:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-8:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-9:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-10:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232"
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-11:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":1,
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type":0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-12:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":1,
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-13:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":"1a",
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-14:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-15:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": 2232,
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-16:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":1
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 99,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-18:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-19:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": "0a",
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-20:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-21:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": 1,
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-23:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": 1,
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-24:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "Z",
//                     "value": "2"
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-25:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":46,
//             "description":{
//                 "scenarioName":"liver transplant",
//                 "patientName":"sarmad abbasi real part2",
//                 "masterManikinId":1,
//                 "dob":"2020-09-10T07:18:08.457Z",
//                 "mrn": "2232",
//                 "hospitalName":"Paras"
                
//             },
//             "type": 0,
//             "labTest": [
//                 {
//                     "criticalValue": "<= 6.0 g/dl, >= 20g/dl",
//                     "labTestName": "hgb",
//                     "date": "2020-10-17T08:32:45.000Z",
//                     "value": 2
//                 }
//             ]
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })


// })