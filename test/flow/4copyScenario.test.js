// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// require('./3createScenario.test');
// const api = supertest('http://localhost:5000/copyscenario');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// const expect=chai.expect;
// const {getToken, getScenarioId} = require('../../models/getToken')

// describe('Testing Copy Scenario:',()=>{
//     var result ;
//     var token ;
//     var scId;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         scId = await getScenarioId();
//         const postObject={
//             "scenarioId" : scId[0].id,
//             "scenarioName":"test102"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.string();
//     })

//     it('subtest-2:should return status 406 ',async ()=>{
        
//         const postObject={
//             "scenarioId" : "46a",
//             "scenarioName":"Hello Honey Bunny"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId" : 46,
//             "scenarioName":1
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"Hello Honey Bunny"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId" : 46
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioIds" : 46,
//             "scenarioName":"Hello Honey Bunny"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId" : 46,
//             "scenarioNames":"Hello Honey Bunny"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })


// })