'use strict'
const supertest = require('supertest');
const chai = require('chai');
const api = supertest('http://localhost:5000/login');
const asserttype = require('chai-asserttype');
chai.use(asserttype);
const expect=chai.expect;

describe('Testing User Login:',()=>{
    it('subtest-1:should return status 200 with credentials',async ()=>{
        
        const postObject={"user":{
            "email":"user@unittest.com",
            "password":"nurse"
            }}
        const res=await api.post('/').send(postObject).expect(200);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(true);
        expect(jsonRes.result).to.be.object();
        expect(jsonRes.result.sessionToken).to.be.string();
        expect(jsonRes.result.user).to.be.object();
        expect(jsonRes.result.user.userId).to.be.number();
        expect(jsonRes.result.user.firstName).to.be.string();
        expect(jsonRes.result.user.lastName).to.be.string();
        expect(jsonRes.result.user.isActive).to.be.number();
        expect(jsonRes.result.role).to.be.object();
        expect(jsonRes.result.role.roleId).to.be.number();
        expect(jsonRes.result.role.roleName).to.be.string();
        expect(jsonRes.result.priviliges).to.be.object();
        expect(jsonRes.result.priviliges.priviliges).to.be.string();
    })

    it('subtest-3:should return status 406 with validation error for user',async ()=>{
        
        const postObject={
            "email":"trainer",
            "password":"admin123"
            }
        const res=await api.post('/').send(postObject).expect(406);
    })

    it('subtest-4:should return status 406 with validation error for email',async ()=>{
        
        const postObject={ "user":{
            "password":"admin123"
            }
        }
        const res=await api.post('/').send(postObject).expect(406);
    })

    it('subtest-5:should return status 406 with validation error for password',async ()=>{
        
        const postObject={ "user":{
            "email":"trainer"
            }
        }
        const res=await api.post('/').send(postObject).expect(406);
    })

    it('subtest-6:should return status 406 with validation error for invalid user',async ()=>{
        
        const postObject={ "user":{
            "email":"trainer",
            "password":"admin12"
            }
        }
        const res=await api.post('/').send(postObject).expect(406);
        const jsonRes=JSON.parse(res.text);
        expect(jsonRes.status).to.be.equal(false);
    })

    it('subtest-7:should return status 406 with validation error for invalid email',async ()=>{
        
        const postObject={ "user":{
            "email":1,
            "password":"admin12"
            }
        }
        const res=await api.post('/').send(postObject).expect(406);
    })

    it('subtest-8:should return status 406 with validation error for invalid password',async ()=>{
        
        const postObject={ "user":{
            "email":"trainer",
            "password":1
            }
        }
        const res=await api.post('/').send(postObject).expect(406);
    })

    it('subtest-9:should return status 406 with validation error for invalid password and email',async ()=>{
        
        const postObject={ "user":{
            "email":1,
            "password":1
            }
        }
        const res=await api.post('/').send(postObject).expect(406);
    })
})