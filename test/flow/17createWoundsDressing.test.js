'use strict'
const supertest = require('supertest');
const chai = require('chai');
const api = supertest('http://localhost:5000/postwoundsdressing');
const asserttype = require('chai-asserttype');
chai.use(asserttype);
require('./16woundsDressing.test');
const expect=chai.expect;
const {getToken, getScenarioId} = require('../../models/getToken');
const { get } = require('lodash');

describe('Testing Create Patient Wounds Dressing:',()=>{
    var result ;
    var token ;
    var scId ;
    const userId = 14;
    // it('subtest-1:should return status 200 with data',async ()=>{
    //     result = await getToken(userId);
    //     token = result[0].token;
    //     scId = await getScenarioId();
    //     const postObject={
    //         "scenarioId":scId[0].id,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(200);
    //     const jsonRes=JSON.parse(res.text);
    //     expect(jsonRes.status).to.be.equal(true);
    //     expect(jsonRes.message).to.be.string();
    // })

    // it('subtest-2:should return status 406 ',async ()=>{
        
    //     const postObject=
    //         {
    //             "description":{
    //                 "scenarioName":"liver transplant",
    //                 "patientName":"sarmad abbasi real part2",
    //                 "masterManikinId":1,
    //                 "dob":"2020-09-10T07:18:08.457Z",
    //                 "mrn": "2232",
    //                 "hospitalName":"Paras"
                    
    //             },
    //             "woundsDressing":[
    //                 {
    //                     "id":null,
    //                     "type":0,
    //                     "location":"Head",
    //                     "appearance":"3"
    //                 },
    //                 {
    //                     "id":null,
    //                     "type":0,
    //                     "location":"Head",
    //                     "appearance":"3"
    //                 }
    //             ]
    //         }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":"46a",
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-7:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-8:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-9:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-10:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232"
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-11:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":1,
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-12:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":1,
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-13:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":"1a",
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-14:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-15:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": 2232,
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-16:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":1
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
    //         }
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })

    // it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":1,
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })
    // it('subtest-17:should return status 406 with validation error for scenarioId',async ()=>{
        
    //     const postObject={
    //         "scenarioId":46,
    //         "description":{
    //             "scenarioName":"liver transplant",
    //             "patientName":"sarmad abbasi real part2",
    //             "masterManikinId":1,
    //             "dob":"2020-09-10T07:18:08.457Z",
    //             "mrn": "2232",
    //             "hospitalName":"Paras"
                
    //         },
    //         "woundsDressing":[
    //             {
    //                 "id":null,
    //                 "type":"0a",
    //                 "location":"Head",
    //                 "appearance":"3"
    //             },
    //             {
    //                 "id":null,
    //                 "type":0,
    //                 "location":"Head",
    //                 "appearance":"3"
    //             }
    //         ]
    //     }
    //     const res=await api.post('/')
    //     .set('Authorization', token)
    //     .set('userId', userId)
    //     .send(postObject).expect(406);
    // })
})