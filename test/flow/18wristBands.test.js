// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/wristbands');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// require('./17createWoundsDressing.test');
// const expect=chai.expect;
// const {getToken} = require('../../models/getToken')

// describe('Testing Wrist Bands:',()=>{
//     var result ;
//     var token ;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         const postObject={
//             "scenarioId":46
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.object();
//     })

//     it('subtest-2:should return status 200 with response status be false ',async ()=>{
        
//         const postObject={
//             "scenarioId":999
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(false);
//     })

//     it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioId":"as"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioIds":46
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={}
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

// })