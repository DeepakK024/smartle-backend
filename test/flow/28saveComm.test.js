// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// const api = supertest('http://localhost:5000/postUserCommunication');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// require('./27updateSessionCommunication.test');
// const expect=chai.expect;
// const {getToken, getSessionId} = require('../../models/getToken')

// describe('Testing Post User Communication:',()=>{
//     var result ;
//     var token ;
//     var sessionId;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         sessionId = await getSessionId(userId)
//         const postObject={
//             "sessionId" : sessionId[0].id,
//             "communicationData" : [
//                 {
//                     "response":{
//                       "options" : [
//                               {
//                               "opt" : "1/2 a pack a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "A pack a day",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Two packs a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "Only when I drink",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Only when I socialize",
//                               "isDefault" : false         
//                               }
//                           ],
//                           "selected" : {
//                               "opt" : "I dont know anything"
//                           }
//                       },
//                       "masterCommunicationId" : 4,
//                       "userID" : 3
//                 }
//             ]
//           }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//     })

//     it('subtest-3:should return status 406 ',async ()=>{
        
//         const postObject={
//             "communicationData" : [
//                 {
//                     "response":{
//                       "options" : [
//                               {
//                               "opt" : "1/2 a pack a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "A pack a day",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Two packs a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "Only when I drink",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Only when I socialize",
//                               "isDefault" : false         
//                               }
//                           ],
//                           "selected" : {
//                               "opt" : "I dont know anything"
//                           }
//                       },
//                       "masterCommunicationId" : 4,
//                       "userID" : 3
//                 }
//             ]
//           }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId" : 226
//           }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-7:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId" : "226a",
//             "communicationData" : 
//                 {
//                     "response":{
//                       "options" : [
//                               {
//                               "opt" : "1/2 a pack a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "A pack a day",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Two packs a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "Only when I drink",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Only when I socialize",
//                               "isDefault" : false         
//                               }
//                           ],
//                           "selected" : {
//                               "opt" : "I dont know anything"
//                           }
//                       },
//                       "masterCommunicationId" : 4,
//                       "userID" : 3
//                 }
//           }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-8:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionId" : 226,
//             "communicationDatas" : [
//                 {
//                     "response":{
//                       "options" : [
//                               {
//                               "opt" : "1/2 a pack a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "A pack a day",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Two packs a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "Only when I drink",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Only when I socialize",
//                               "isDefault" : false         
//                               }
//                           ],
//                           "selected" : {
//                               "opt" : "I dont know anything"
//                           }
//                       },
//                       "masterCommunicationId" : 4,
//                       "userID" : 3
//                 }
//             ]
//           }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-9:should return status 406 ',async ()=>{
        
//         const postObject={
//             "sessionIds" : 226,
//             "communicationData" : [
//                 {
//                     "response":{
//                       "options" : [
//                               {
//                               "opt" : "1/2 a pack a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "A pack a day",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Two packs a day",
//                               "isDefault" : false
//                               },
//                               {
//                               "opt" : "Only when I drink",
//                               "isDefault" : false         
//                               },
//                               {
//                               "opt" : "Only when I socialize",
//                               "isDefault" : false         
//                               }
//                           ],
//                           "selected" : {
//                               "opt" : "I dont know anything"
//                           }
//                       },
//                       "masterCommunicationId" : 4,
//                       "userID" : 3
//                 }
//             ]
//           }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
// })