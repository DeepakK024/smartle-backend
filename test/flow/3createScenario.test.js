// 'use strict'
// const supertest = require('supertest');
// const chai = require('chai');
// require('./2scenarioList.test')
// const api = supertest('http://localhost:5000/createscenerio');
// const asserttype = require('chai-asserttype');
// chai.use(asserttype);
// const expect=chai.expect;
// const {getToken} = require('../../models/getToken')

// describe('Testing Create Scenario:',()=>{
//     var result ;
//     var token ;
//     const userId = 14;
//     it('subtest-1:should return status 200 with data',async ()=>{
//         result = await getToken(userId);
//         token = result[0].token;
//         const postObject={
//             "scenarioName":"test101",
//             "patientName":"Deepak kumar",
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(200);
//         const jsonRes=JSON.parse(res.text);
//         expect(jsonRes.status).to.be.equal(true);
//         expect(jsonRes.result).to.be.number();
//     })

//     it('subtest-2:should return status 406 ',async ()=>{
        
//         const postObject={
//             "scenarioName":"A new Scenario",
//             "patientName":"Deepak kumar",
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-3:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"A new Scenario",
//             "patientName":"Deepak kumar",
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-4:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"A new Scenario",
//             "patientName":"Deepak kumar",
//             "masterManikinId":1,
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-5:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"A new Scenario",
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"A new Scenario",
//             "patientName":"Deepak kumar",
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "patientName":"Deepak kumar",
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":1,
//             "patientName":"Deepak kumar",
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"as",
//             "patientName":1,
//             "masterManikinId":1,
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"as",
//             "patientName":"as",
//             "masterManikinId":"1a",
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"as",
//             "patientName":"as",
//             "masterManikinId":"1",
//             "dob":"2asa",
//             "mrn": "2232",
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"as",
//             "patientName":"as",
//             "masterManikinId":"1",
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": 2232,
//             "hospitalName":"Paras"
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })

//     it('subtest-6:should return status 406 with validation error for scenarioId',async ()=>{
        
//         const postObject={
//             "scenarioName":"as",
//             "patientName":"as",
//             "masterManikinId":"1",
//             "dob":"2020-09-10T07:18:08.457Z",
//             "mrn": "2232",
//             "hospitalName":1
//         }
//         const res=await api.post('/')
//         .set('Authorization', token)
//         .set('userId', userId)
//         .send(postObject).expect(406);
//     })
// })